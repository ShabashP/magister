\subsection{Система управления движением мобильного робота по принципу слежения за программной траекторией}
\label{subsection42}

\par Для управления движением патрулирующего робота используется система слежения за программной траекторией.
Структурная схема такой системы представлена на рисунке \ref{img:control_schema}.
Функциональное и алгоритмическое описание блоков структурной схемы представлено ниже.

\begin{figure}[H]
\centering
\includegraphics[scale=0.65]{section_4/img/control_schema.png}
\caption{Структурная схема системы слежения за программной траекторией}
\label{img:control_schema}
\end{figure}


\paragraph{Синтез программной траектории}\mbox{}

\par Программная траектория задается в виде \ref{program:desired}.
Подробное описание процесса синтеза программных траекторий различного вида представлено в главе \ref{section03}.

\par Традиционно в литературе различают две постановки задачи слежения за программной траекторией:
временную \cite{koh} и траекторную \cite{burdakov02}.
Временная постановка предусматривает слежение за текущей точкой программной траектории,
а траекторная постановка --- за ближайшей к текущему положению робота.
\par Для оценки качества слежения за программной траекторией вычисляются траекторные ошибки.
Для временной постановки рассчитываются три ошибки, а для траекторной --- две
(ошибка слежения вдоль программной траектории не является существенной). Кроме того,
для траекторной постановки задачи слежения характерными являются эффекты ''ухода'' текущей точки
программной траектории от реального положения робота \cite{burdakov}.
Поэтому в дальнейшем при описании блоков, составляющих систему слежения за программной траекторией,
и при проведении вычислительных экспериментов рассматривается только траекторная постановка.

\paragraph{Параметризация программной траектории}\mbox{}

\par Программная траектория может быть представлена в параметрическом виде в зависимости от параметра
$p \in \left[ 0, t^r \right]$, где $t^r$ --- программное время движения.
Тогда ближайшей к текущему положению робота точке программной траектории соответствует параметр
%\par Траекторная постановка задачи предусматривает слежение
%за ближайшей к текущему положению робота точкой программной траектории. Данная точка определяется
%желаемым состоянием робота в момент времени $t^*$:
%
%\[
%    t^* (\hat{x}, \hat{y}) = \text{arg} \min_{i \in \left[ 0, N \right]} \{ (\hat{x} - x^r_i)^2 + (\hat{y} - y^r_i)^2 \},
%\]
\[
    p_* (\hat{x}, \hat{y}) = \text{arg} \min_{p \in \left[ 0, t^r \right]} \{ (\hat{x} - x^r (p))^2 + (\hat{y} - y^r (p))^2 \},
\]
\noindent где $ \hat{x}, \hat{y} $ --- оценки текущих координат точки, принятой за полюс робота.

\par Зная значение $p_*$ в каждый момент времени можно получить все необходимые параметры желаемого состояния робота:

\[
\begin{array}{l}

x^r_* = x^r (t) |_{t = p_*}, \\
y^r_* = y^r (t) |_{t = p_*}, \\
\varphi^r_* = \varphi^r (t) |_{t = p_*}, \\
V^r_* = V^r (t) |_{t = p_*}, \\
\Omega^r_* = \Omega^r (t) |_{t = p_*}.
\end{array}
\]

\par В общем случае в зависимости от вида программной траектории полюс робота может не совпадать с центром масс $C$.
Координаты полюса, лежащего на продольной оси робота, в опорной системе координат имеют вид

\begin{equation}
\label{polus:coord}
\begin{array}{l}

    x = x_C + x' \cos \varphi, \\
    y = y_C + x' \sin \varphi,

\end{array}
\end{equation}

\noindent где $x' = l - L^R$; $l$ --- расстояние от оси ведущих колес до полюса.

\par Проекции скорости полюса на подвижные оси, связанные с с центром масс робота $C$, определяются следующим образом:

\begin{equation}
\label{polus:speed}
\begin{array}{l}

    V'_1 = V_1, \\
    V'_2 = V_2 + \Omega x'.

\end{array}
\end{equation}

\paragraph{Расчет траекторных ошибок}\mbox{}

\par Ошибки положения мобильного робота в подвижной системе координат, связанной с ближайшей точкой программной траектории
представлены на рисунке \ref{img:errors}.
\par Слежение за программной траекторие осуществляется так, чтобы продольная ось робота была направлена по касательной
к траектории в ближайшей к текущему положению точке.

\begin{figure}[H]
\centering
\includegraphics[scale=0.65]{section_4/img/errors.png}
\caption{Ошибки положения робота для траекторной постановки задачи слежения}
\label{img:errors}
\end{figure}

\par Соответствующие выражения имеют вид

\begin{equation}
\label{errors}
\begin{array}{l}

    e_y = - (x^r_* - \hat{x}) \sin \varphi^r_* + (y^r_* - \hat{y}) \cos \varphi^r_*, \\
    e_{\varphi} = \varphi^r_* - \hat{\varphi},

\end{array}
\end{equation}

\noindent где $e_y$ --- ошибка по нормали к траектории; $e_{\varphi}$ --- ошибка ориентации.

\paragraph{Расчет заданий на скорости}\mbox{}

\par В данном блоке осуществляется расчет желаемых линейной скорости полюса $V^d$ и уголовой скорости $\Omega^d$ робота
с учетом траекторных ошибок \ref{errors}.
\par Блок \textit{Расчет заданий на скорости} является основным блоком всей системы слежения за программной траекторией,
от алгоритмического наполнения которого зависит ее эффективность.
\par В дальнейшем при моделировании использовались простейшие пропорциональные алгоритмы расчета $V^d$ и $\Omega^d$,
однако в литературе представлены другие (например, логико-лингвистические \cite{burdakov}) алгоритмы расчета данных величин.

\par Желаемая линейная скорость полюса робота определяется следующим образом:

\begin{equation}
\label{robot:speed}
\begin{array}{ll}
V^d = V^r_* \left( 1 - K_V \dfrac{|e_\varphi|}{\pi} \right), & K_V \in \left[0, 1 \right],
\end{array}
\end{equation}

\noindent где ошибка ориентации $e_\varphi$ нормализована так, что $e_\varphi \in \left[-\pi, \pi \right]$;
значение коэффициента $K_V$ определяет уровень снижения программной скорости при маневрировании.

\par Желаемая угловая скорость определяется следующим образом:


\begin{equation}
\label{robot:angular_speed}
  \Omega^d = \Omega^r_* + K_y e_y + K_\varphi e_\varphi,
\end{equation}

\noindent где $K_y$ --- коэффициент обратной связи по отклонению от траекториии;
$K_\varphi$  --- коэффициент обратной связи по ошибке ориентации.


\paragraph{Расчет заданий для приводов }\mbox{}

\par В данном блоке желаемые линейная скорость полюса $ V^d $ и уголовая скорость $ \Omega^d $ робота пересчитываются
в задания для приводов.
С учетом уравнения рулевого привода \ref{wheel_turn} и автомобильной компоновки шасси робота
преобразование осуществляется следующим образом:

\begin{equation}
\label{privod_controls}
\renewcommand{\arraystretch}{2.0}
\begin{array}{l}

    \omega^d = \dfrac{V^d}{r^R \left( 1 + \left( \dfrac {l \tan \beta^d}{L} \right)^2 \right)^\frac{1}{2}}, \\
    \beta ^d = \text{sat} \left( \arctan \dfrac{\Omega^d L}{V^d}, \beta_{max}  \right)

\end{array}
\end{equation}

\paragraph{Управление приводами}\mbox{}

\par Данный блок состоит из независимых серворегуляторов, осуществляющих управление приводами по рассогласованиям желаемых
и текущих значений переменных, характеризующих состояние робота.
\par Для привода рулевого механизма принят релейный регулятор \ref{wheel_turn}.
Для управления двигателем привода ведущих колес используется PI-регулятор с компенсацией
противо-ЭДС:

\begin{equation}
\label{engine_controls}
u = u = - K_P \left( \omega^R - \omega^d \right) - K_I \int \limits_0^t \left( \omega^R - \omega^d \right) dt + \dfrac{h}{d} \omega,
\end{equation}

\noindent где $K_P$ и $K_I$ --- коэффициенты регулятора.

\par Выбор и оптимизация коэффициентов регулятора осуществляется в предположениии отсутствия проскальзывания колес.

\paragraph{Оценка скоростей и координат}\mbox{}

\par В данном блоке производится вычисление оценок текущих скоростей и координат полюса робота на основе измерений
с датчиков обратных связей. Учитывая, что движение автономного робота происходит в условиях неконтролируемых возмущений,
в этом блоке периодически должна осуществляться коррекция оценок скоростей и координат оператором или автоматически
по показаниям независимых измерителей, например системы спутниковой навигации.

\par Скорости полюса робота расчитываются по формулам \ref{polus:speed}. Координаты определяются в соответствии
с \ref{robot:dynamic} по следующим формулам:

\begin{equation}
\renewcommand{\arraystretch}{1.8}
\label{hat_coord}
\begin{array}{l}
x = x (t_0) + \int \limits_{t_0}^t \left( V'_1 \cos \varphi - V'_2 \sin \varphi \right) dt, \\
y = y (t_0) + \int \limits_{t_0}^t \left( V'_1 \sin \varphi + V'_2 \cos \varphi \right) dt, \\
\varphi = \varphi(t_0) + \int \limits_{t_0}^t \Omega dt,
\end{array}
\end{equation}

\noindent где $t_0$ --- момент времени очередной коррекции;
$x (t_0), y (t_0), \varphi (t_0)$ --- координаты и ориентация робота на момент коррекции,
 полученные с помощью независимых измерителей.

\par Оценки величин $V_1$, $V_2$ и $\Omega$, входящих в уравнения \ref{polus:speed} и далее в \ref{hat_coord},
определяются в предположении отсутствия проскальзывания колес:

\begin{equation}
\label{hat_speed}
\renewcommand{\arraystretch}{1.5}
\begin{array}{l}
\hat{V}_1 = \omega^R r^R, \\
\hat{V}_2 = L^R \hat{\Omega}, \\
\hat{\Omega} = \dfrac{\omega^R r^R}{L} \tan \beta,
\end{array}
\end{equation}

\noindent где $\omega^R$, $\beta$ --- сигналы с датчиков обратных связей.
