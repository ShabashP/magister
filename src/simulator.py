# coding=utf-8
import numpy as np
from collections import deque

from src.kernel.track.maps import Map
from src.kernel.objects.car import RealCar
from src.kernel.friction import TrackFrictionApproximation
from src.kernel.world import TIME_SUB_STEP


class Collision(Exception):
    pass


class Simulator(object):

    def __init__(self, track_map, initial_state, friction, pass_distance):
        assert isinstance(track_map, Map)
        assert isinstance(friction, TrackFrictionApproximation), "Bad friction"

        self.track = track_map

        state = np.zeros((10, ), dtype=np.float)
        state[0:3] = initial_state

        self.pass_distance = pass_distance
        self.car = RealCar("Car", *state, friction=friction)

        self.waypoints = deque(track_map.waypoints)
        self.waypoints.append(track_map.waypoints[1])
        self.data = np.zeros((12, ), dtype=np.float)
        self.data[1:4] = initial_state
        self.data[-1] = np.inf
        self.step = 0
        self.duration = -1.0

    @property
    def is_finished(self):
        return len(self.waypoints) == 0

    @property
    def time(self):
        return self.data[:, 0]

    @property
    def x(self):
        return self.data[:, 1]

    @property
    def y(self):
        return self.data[:, 2]

    @property
    def angle(self):
        return self.data[:, 3]

    @property
    def speed(self):
        return np.hypot(self.data[:, 4], self.data[:, 5])

    @property
    def speed_lengthwise(self):
        return self.data[:, 4]

    @property
    def speed_crosswise(self):
        return self.data[:, 5]

    @property
    def angular_speed(self):
        return self.data[:, 6]

    @property
    def omega_r(self):
        return self.data[:, 7]

    @property
    def omega_f(self):
        return self.data[:, 8]

    @property
    def torque(self):
        return self.data[:, 9]

    @property
    def wheel_turn(self):
        return self.data[:, 10]

    @property
    def min_distance(self):
        return self.data[:, 11]

    def _sub_step(self, moment, wheel_turn):

        # j, i = get_tile_index(self.car.x, self.car.y)

        # print "%s != %s" % (get_tile_index(self.car.x, self.car.y), self.waypoints[0])

        if np.hypot(self.car.x-self.waypoints[0][0], self.car.y-self.waypoints[0][1]) <= self.pass_distance:
            # print "Waypoint (%s, %s) passed" % (self.waypoints[0][0], self.waypoints[0][1])
            self.waypoints.popleft()
            if len(self.waypoints) == 1:
                self.duration = self.time[-1]

        if not self.car.move(self.car.integrator.t + TIME_SUB_STEP, moment, wheel_turn):
            raise Exception("Integration failed")

        self.data = np.vstack((
            self.data,
            np.hstack(
                (
                    self.car.integrator.t,
                    self.car.state,
                    float(self.track.barrier_distance(self.car.x, self.car.y))
                )
            )
        ))

        if self.track.collide_object(self.car):
            raise Collision(
                "Collision handled (car: %s, x: %s, y: %s)" % (
                    self.car.id, self.car.x, self.car.y
                )
            )

        self.step += 1

    def simulate(self, moment, wheel_turn):

        self._sub_step(moment, wheel_turn)

        # if self.step % 500 == 0:
        #     print "Simulation time %s \t x=%s \t y=%s \t speed=%s" % (
        #         self.car.integrator.t, self.car.x, self.car.y, self.speed[-1]
        #     )

        return self.car.integrator.t, self.car.x, self.car.y, self.car.angle, self.car.omega_r, self.car.wheel_turn
