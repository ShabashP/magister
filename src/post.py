# coding=utf-8
import numpy as np
from src.controller import Controller
from src.simulator import Simulator


class PostProcessor(object):
    def __init__(self, simulator, controller, success):
        """
        :type simulator: Simulator
        :type controller: Controller
        """

        # assert len(simulator.time) == len(controller.time), "Data len not equal"

        self.success = success
        self.time = simulator.time

        wtx, wty = np.cos(simulator.wheel_turn), np.sin(simulator.wheel_turn)

        alf = simulator.speed_crosswise + simulator.angular_speed * simulator.car.arm_forward

        # Скорости колес
        v1_f = simulator.speed_lengthwise * wtx + alf * wty  # продольная скорость передних колес
        self.vs2_f = alf * wtx - simulator.speed_lengthwise * wty  # поперечная скорость передних колес
        v1_r = simulator.speed_lengthwise  # продольная скорость задних колес
        self.vs2_r = simulator.speed_crosswise - simulator.angular_speed * simulator.car.arm_rear  # поперечная скорость задних колес

        # Радиус * скорость вращения
        omega_radius_f = simulator.omega_f * simulator.car.wheel_forward_radius  # передних
        omega_radius_r = simulator.omega_r * simulator.car.wheel_rear_radius  # задних

        # Продольные скорости скольжения колес
        self.vs1_f = v1_f - omega_radius_f  # передних
        self.vs1_r = v1_r - omega_radius_r  # задних

        # Максимальные величины скольжения
        self.max_vs1_f = float(np.abs(self.vs1_f).max())
        self.max_vs1_r = float(np.abs(self.vs1_r).max())
        self.max_vs2_f = float(np.abs(self.vs2_f).max())
        self.max_vs2_r = float(np.abs(self.vs2_r).max())

        # Средние величины скольжения
        self.av_vs1_f = float(np.abs(self.vs1_f).mean())
        self.av_vs1_r = float(np.abs(self.vs1_r).mean())
        self.av_vs2_f = float(np.abs(self.vs2_f).mean())
        self.av_vs2_r = float(np.abs(self.vs2_r).mean())

        # Максимальные и средние ошибки
        self.max_e_angle = float(np.abs(controller.e_angle).max())
        self.max_e_coord = float(np.abs(controller.e_coord).max())
        self.av_e_angle = float(np.abs(controller.e_angle).mean())
        self.av_e_coord = float(np.abs(controller.e_coord).mean())

        # Время симуляции
        self.simulation_time = float(simulator.duration)
        self.program_time = float(controller.program_time[-1])
        self.time_over = self.simulation_time / self.program_time if success else np.inf

        # Максимальный момент
        self.M_max = simulator.car.M_max * np.ones_like(simulator.time)

        # Максимальные углы поворота руля
        self.max_wheel_turn = simulator.car.max_wheel_turn * np.ones_like(simulator.time)

        # Коэффициенты регуляторов
        self.K_p = controller.K_p
        self.K_v = controller.K_v
        self.K_i = controller.K_i
        self.K_coord = controller.K_coord
        self.K_angle = controller.K_angle

        # Минимальное расстояние
        self.min_d = simulator.min_distance.min()