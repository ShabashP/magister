# coding=utf-8
import time
from os import pardir
from os.path import isdir, abspath, join
from functools import partial

from src.kernel.plot import *
from src.kernel.track.maps import Map
from src.kernel.friction import TrackFrictionApproximation

from src.post import PostProcessor
from src.controller import Controller
from src.synthesizer import Synthesizer, ProgramTrajectory, BaseProgramTrajectory
from src.simulator import Simulator, Collision


def calculate(arguments):
    track_map, trajectory, initial_state, friction, min_pass_distance, out_dir, friction_name, k_v = arguments
    assert isdir(out_dir), "Bad output directory: %s" % out_dir
    assert isinstance(trajectory, ProgramTrajectory), "Bad trajectory type"
    assert isinstance(track_map, Map), "Bad map type"
    assert isinstance(friction, TrackFrictionApproximation), "Bad friction type"

    title = "%s___%s" % (trajectory.identificator, friction_name)
    t_start = time.time()
    simulator = Simulator(track_map, initial_state, friction, min_pass_distance)
    controller = Controller(trajectory, k_v)

    t0 = time.time()
    controls = controller.get_controls(0.0, initial_state[0], initial_state[1], initial_state[2], 0.0)
    success = True
    while not simulator.is_finished:
        try:
            args = simulator.simulate(*controls)
            controls = controller.get_controls(*args)
        except (Collision, KeyboardInterrupt) as e:
            success = False
            print "Error: %s" % e
            break
    calculation_time = time.time() - t0
    p = PostProcessor(simulator, controller, success)
    save = partial(save_figure, out_dir)

    # ----------------------------------------------------------------------------------------------------------------
    # Plot trajectory
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    picture.plot(controller.x, controller.y, "k--", linewidth=1.25)
    picture.plot(simulator.x, simulator.y, DEFAULT_LINE, linewidth=LINE_WIDTH)
    if not simulator.is_finished:
        simulator.car.plot_mpl(picture, linestyle="r-")
    track_map.mpl_draw(picture, ticks=True, labels=False, waypoints_order=False)
    # picture.legend(["desired", "real"], loc=4)
    save_figure(abspath(join(out_dir, pardir)) + "\\", figure, title, False)
    save(figure, "01_trajectory")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot car speed
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = plt.subplots(3, sharex=True)
    picture[0].set_title("Car lengthwise speed", fontsize=FONT_SIZE)
    picture[0].plot(simulator.time, simulator.speed_lengthwise, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[0].set_ylabel(r"$V_1$", fontsize=FONT_SIZE)
    picture[1].set_title("Car crosswise speed", fontsize=FONT_SIZE)
    picture[1].plot(simulator.time, simulator.speed_crosswise, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[1].set_ylabel(r"$V_2$", fontsize=FONT_SIZE)
    picture[2].set_title("Car angular speed", fontsize=FONT_SIZE)
    picture[2].plot(simulator.time, simulator.angular_speed, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[2].set_ylabel(r"$\Omega$", fontsize=FONT_SIZE)
    picture[2].set_xlabel("time", fontsize=FONT_SIZE)
    grid_for_axis(picture)
    save(figure, "02_car_speed")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot speed
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = plt.subplots(2, sharex=True)
    picture[0].set_title("Speed", fontsize=FONT_SIZE)
    picture[0].plot(controller.program_time, controller.speed, EXT_LINE, linewidth=LINE_WIDTH)
    picture[0].plot(controller.time, controller.speed_d, THIRD_LINE, linewidth=LINE_WIDTH)
    picture[0].plot(simulator.time, simulator.speed, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[0].legend(["program", "desired", "real"])
    picture[1].set_title("Angular speed", fontsize=FONT_SIZE)
    picture[1].plot(controller.program_time, controller.angular_speed, EXT_LINE, linewidth=LINE_WIDTH)
    picture[1].plot(controller.time, controller.angular_speed_d, THIRD_LINE, linewidth=LINE_WIDTH)
    picture[1].plot(simulator.time, simulator.angular_speed, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[1].legend(["program", "desired", "real"])
    picture[1].set_xlabel("time", fontsize=FONT_SIZE)
    grid_for_axis(picture)
    save(figure, "03_speed")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot errors
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = plt.subplots(2, sharex=True)
    picture[0].set_title("Errors", fontsize=FONT_SIZE)
    picture[0].plot(controller.time, controller.e_coord, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[0].set_ylabel(r"$e_y$", fontsize=FONT_SIZE)
    picture[1].plot(controller.time, controller.e_angle, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[1].set_ylabel(r"$e_{\phi}$", fontsize=FONT_SIZE)
    picture[1].set_xlabel("time", fontsize=FONT_SIZE)
    grid_for_axis(picture)
    save(figure, "04_errors")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot sliding speed
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = plt.subplots(2, sharex=True)
    picture[0].set_title("Wheels sliding speed", fontsize=FONT_SIZE)
    picture[0].plot(p.time, p.vs1_f, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[0].plot(p.time, p.vs1_r, EXT_LINE, linewidth=LINE_WIDTH)
    picture[0].set_ylabel(r"$V_{S_1}$", fontsize=FONT_SIZE)
    picture[0].legend([r"$V^{F}_{S_1}$", r"$V^{R}_{S_1}$"])
    picture[1].plot(p.time, p.vs2_f, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[1].plot(p.time, p.vs2_r, EXT_LINE, linewidth=LINE_WIDTH)
    picture[1].set_ylabel(r"$V_{S_2}$", fontsize=FONT_SIZE)
    picture[1].set_xlabel("time", fontsize=FONT_SIZE)
    picture[1].legend([r"$V^{F}_{S_2}$", r"$V^{R}_{S_2}$"])
    grid_for_axis(picture)
    save(figure, "05_sliding_speed")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot controls
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = plt.subplots(3, sharex=True)
    picture[0].set_title("Controls", fontsize=FONT_SIZE)
    picture[0].plot(controller.time, controller.u, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[0].plot(simulator.time, p.M_max, "k--", linewidth=LINE_WIDTH)
    picture[0].set_ylabel(r"$M=du$", fontsize=FONT_SIZE)
    picture[1].plot(simulator.time, simulator.torque, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[1].set_ylabel(r"$\tau$", fontsize=FONT_SIZE)
    picture[2].plot(controller.time, controller.wheel_turn_d, EXT_LINE, linewidth=LINE_WIDTH)
    picture[2].plot(simulator.time, simulator.wheel_turn, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture[2].plot(simulator.time, p.max_wheel_turn, "k--", linewidth=LINE_WIDTH)
    picture[2].plot(simulator.time, -p.max_wheel_turn, "k--", linewidth=LINE_WIDTH)
    picture[2].set_ylabel(r"$\beta$", fontsize=FONT_SIZE)
    picture[2].set_xlabel("time", fontsize=FONT_SIZE)
    picture[2].legend([r"$\beta^d$", r"$\beta$"])
    grid_for_axis(picture)
    save(figure, "06_controls")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot angular speed
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    picture.plot(simulator.time, simulator.angular_speed, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture.plot(controller.program_time, controller.angular_speed, "k--", linewidth=LINE_WIDTH)
    picture.set_ylabel(r"$\Omega$", fontsize=FONT_SIZE)
    picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    picture.legend([r"$\Omega(t)$", r"$\Omega^r(t)$"])
    picture.grid(True)
    picture.set_xlim([0, simulator.duration])
    save(figure, "07_angular_speed")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot e_coord
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    picture.plot(controller.time, controller.e_coord, DEFAULT_LINE, linewidth=ONE_LINE)
    picture.set_ylabel(r"$e_y$", fontsize=FONT_SIZE)
    picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    picture.grid(True)
    picture.set_xlim([0, controller.duration])
    save(figure, "08_e_coord")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot e_angle
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    picture.plot(controller.time, controller.e_angle, DEFAULT_LINE, linewidth=ONE_LINE)
    picture.set_ylabel(r"$e_{\phi}$", fontsize=FONT_SIZE)
    picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    picture.grid(True)
    picture.set_xlim([0, controller.duration])
    save(figure, "09_e_angle")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot v_s1_f
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    picture.plot(p.time, p.vs1_f, DEFAULT_LINE, linewidth=ONE_LINE)
    # picture.set_ylabel(r"$V^{F}_{S_1}$", fontsize=FONT_SIZE)
    # picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    picture.legend([r"$V^{F}_{S_1}(t)$"])
    picture.grid(True)
    picture.set_xlim([0, simulator.duration])
    picture.set_ylim([-p.vs1_f[200:].max(), p.vs1_f[200:].max()])
    save(figure, "10_v_s1_f")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot v_s1_r
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    picture.plot(p.time, p.vs1_r, DEFAULT_LINE, linewidth=ONE_LINE)
    # picture.set_ylabel(r"$V^{R}_{S_1}$", fontsize=FONT_SIZE)
    # picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    picture.legend([r"$V^{R}_{S_1}(t)$"])
    picture.grid(True)
    picture.set_xlim([0, simulator.duration])
    picture.set_ylim([-p.vs1_r[200:].max(), p.vs1_r[200:].max()])
    save(figure, "11_v_s1_r")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot v_s2_f
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    picture.plot(p.time, p.vs2_f, DEFAULT_LINE, linewidth=ONE_LINE)
    # picture.set_ylabel(r"$V^{F}_{S_2}$", fontsize=FONT_SIZE)
    # picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    picture.legend([r"$V^{F}_{S_2}(t)$"])
    picture.set_xlim([0, simulator.duration])
    picture.grid(True)
    save(figure, "12_v_s2_f")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot v_s2_r
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    picture.plot(p.time, p.vs2_r, DEFAULT_LINE, linewidth=ONE_LINE)
    # picture.set_ylabel(r"$V^{R}_{S_2}$", fontsize=FONT_SIZE)
    # picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    picture.legend([r"$V^{R}_{S_2}(t)$"])
    picture.set_xlim([0, simulator.duration])
    picture.grid(True)
    save(figure, "13_v_s2_r")

    print "%s finished in %s seconds (success=%s)" % (title, time.time() - t_start, success)
    return title, calculation_time, p


def base_plot(arguments):
    track_map, synthesizer, trajectories, out_dir = arguments
    assert isinstance(track_map, Map), "Bad map type"
    assert isinstance(synthesizer, Synthesizer), "Bad synthesizer type"
    assert isdir(out_dir), "Bad output directory: %s" % out_dir
    save = partial(save_figure, out_dir)

    # ----------------------------------------------------------------------------------------------------------------
    # Plot map
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    track_map.mpl_draw(picture)
    save(figure, "01_map")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot tiles
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    track_map.mpl_draw(picture, ticks=True, labels=False, waypoints_area=True, waypoints_order=False, barriers=False)
    synthesizer.track.mpl_draw(picture, tile_grid=True, tile_type=True, indices=False)
    save(figure, "02_tiles")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot routes
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    synthesizer.mpl_draw_routes(picture)
    synthesizer.track.mpl_draw(picture, tile_grid=True, tile_type=False, indices=False, tiles=False)
    track_map.mpl_draw(picture, ticks=True, labels=False, waypoints_area=True, waypoints_order=False, barriers=False)
    save(figure, "03_routes")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot best route
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    synthesizer.mpl_draw_best_route(picture)
    synthesizer.track.mpl_draw(picture, tile_grid=True, tile_type=False, indices=False, tiles=False)
    track_map.mpl_draw(picture, ticks=True, labels=False, waypoints_area=True, barriers=False, waypoints_order=False)
    save(figure, "04_route")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot graph
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    synthesizer.mpl_draw_graph(picture)
    save(figure, "05_graph")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot routes on graph
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    synthesizer.mpl_draw_graph_routes(picture)
    save(figure, "06_graph_routes")


def plot_trajectory(arguments):
    track_map, synthesizer, trajectory, out_dir = arguments
    assert isinstance(track_map, Map), "Bad map type"
    assert isinstance(synthesizer, Synthesizer), "Bad synthesizer type"
    assert isinstance(trajectory, ProgramTrajectory)
    assert isdir(out_dir), "Bad output directory: %s" % out_dir
    save = partial(save_figure, out_dir)

    # ----------------------------------------------------------------------------------------------------------------
    # Plot program trajectory
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    picture.plot(trajectory.x, trajectory.y, "k-")
    for i in xrange(len(synthesizer.best_route)-1):
        picture.plot(
            [synthesizer.best_route[i][0], synthesizer.best_route[i+1][0]],
            [synthesizer.best_route[i][1], synthesizer.best_route[i+1][1]],
            "k--"
        )
        picture.plot(synthesizer.best_route[i][0], synthesizer.best_route[i][1], "ko", markersize=5.0)
    # picture.legend(["trajectory", "route"], loc=4)
    track_map.mpl_draw(picture, ticks=True, labels=False, waypoints_area=True, barriers=False, waypoints_order=False)
    synthesizer.track.mpl_draw(picture, tile_grid=True, tile_type=False, indices=False, tiles=False)
    save(figure, "01_trajectory")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot x^r (t)
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()

    if isinstance(trajectory, BaseProgramTrajectory):
        picture.plot(trajectory.time, trajectory.x, DEFAULT_MARKER, linewidth=LINE_WIDTH, markersize=MARKER_SIZE)
    else:
        picture.plot(trajectory.time, trajectory.x, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture.grid(True)
    picture.set_ylabel(r"$x^r$", fontsize=FONT_SIZE)
    picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    picture.set_xlim([0, trajectory.duration])
    save(figure, "02_x")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot y^r (t)
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()

    if isinstance(trajectory, BaseProgramTrajectory):
        picture.plot(trajectory.time, trajectory.y, DEFAULT_MARKER, markersize=MARKER_SIZE)
    else:
        picture.plot(trajectory.time, trajectory.y, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture.grid(True)
    picture.set_ylabel(r"$y^r$", fontsize=FONT_SIZE)
    picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    picture.set_xlim([0, trajectory.duration])
    save(figure, "03_y")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot angle^r (t)
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()
    if isinstance(trajectory, BaseProgramTrajectory):
        picture.plot(trajectory.time, trajectory.angle, DEFAULT_MARKER, markersize=MARKER_SIZE)
    else:
        picture.plot(trajectory.time, trajectory.angle, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture.grid(True)
    picture.set_xlim([0, trajectory.duration])
    picture.set_ylabel(r"$\varphi^r$", fontsize=FONT_SIZE)
    picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    save(figure, "04_angle")
    # ----------------------------------------------------------------------------------------------------------------
    # Plot program angular speed
    # ----------------------------------------------------------------------------------------------------------------
    figure, picture = new_figure()

    if isinstance(trajectory, BaseProgramTrajectory):
        picture.plot(trajectory.time, trajectory.angular_speed, DEFAULT_MARKER, markersize=MARKER_SIZE)
    else:
        picture.plot(trajectory.time, trajectory.angular_speed, DEFAULT_LINE, linewidth=LINE_WIDTH)
    picture.grid(True)
    picture.set_ylabel(r"$\Omega^r$", fontsize=FONT_SIZE)
    picture.set_xlabel(r"$t$", fontsize=FONT_SIZE)
    picture.set_xlim([0, trajectory.duration])
    save(figure, "05_angular_speed")
