# coding=utf-8
from math import pi


def normalize_relative(a):

    if a > 1.0:
        return 1.0
    elif a < -1.0:
        return -1.0
    return a


def linear_interpolation(x, x1, x2, y1, y2):
    return (x - x1)*(y2 - y1)/(x2 - x1) + y1


def normalize_angle(angle):

    while angle > pi:
        angle -= 2.0 * pi
    while angle < -pi:
        angle += 2.0 * pi

    return angle


def saturate(value, limit):

    if value > limit:
        return limit
    elif value < -limit:
        return -limit
    return value