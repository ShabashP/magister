# coding=utf-8
"""
Модуль синтеза программных траекторий движения
"""
import numpy as np
from copy import copy
import multiprocessing as mp
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
from scipy.interpolate import interp1d
from collections import deque, namedtuple

from src.utils import normalize_angle
from src.kernel.track.maps import Map
from src.kernel.world import TIME_SUB_STEP
from src.kernel.track.tiles import Tile, TileType
from src.kernel.track.track import Track, Maneuvers, get_tile_index
from src.kernel.objects.car import BaseObject, KinematicCar, MaterialPoint

CLEAR_PATH = -1
GRAPH_BASE_RADIUS = 8.0
GRAPH_BASE_OFFSET = 6.0
GRAPH_ALIGN = 2*GRAPH_BASE_RADIUS + GRAPH_BASE_OFFSET
GRAPH_FONT_SIZE = 6.0

# Значения для определения направления начала движения
INITIAL_ANGLES = np.array([i_angle*0.25*np.pi for i_angle in xrange(8)])
INITIAL_WAYS = (
    lambda tile: (tile.index[0], tile.index[1] + 1),
    lambda tile: (tile.index[0] + 1, tile.index[1] + 1),
    lambda tile: (tile.index[0] + 1, tile.index[1]),
    lambda tile: (tile.index[0] + 1, tile.index[1] - 1),
    lambda tile: (tile.index[0], tile.index[1] - 1),
    lambda tile: (tile.index[0] - 1, tile.index[1] - 1),
    lambda tile: (tile.index[0] - 1, tile.index[1]),
    lambda tile: (tile.index[0] - 1, tile.index[1] + 1),
)

# Описание маневров (те тайлы, для которых данный переход запрещен)
Maneuver = namedtuple("Maneuver", ("type_from", "type_to"))
ManeuverUp = Maneuver(
    type_from=(TileType.TOP_BORDER, TileType.LEFT_TOP_ANGLE, TileType.RIGHT_TOP_ANGLE),
    type_to=(TileType.BOTTOM_BORDER, TileType.RIGHT_BOTTOM_ANGLE, TileType.LEFT_BOTTOM_ANGLE)
)
ManeuverDown = Maneuver(
    type_from=ManeuverUp.type_to,
    type_to=ManeuverUp.type_from
)
ManeuverLeft = Maneuver(
    type_from=(TileType.LEFT_BORDER, TileType.LEFT_TOP_ANGLE, TileType.LEFT_BOTTOM_ANGLE),
    type_to=(TileType.RIGHT_BORDER, TileType.RIGHT_BOTTOM_ANGLE, TileType.RIGHT_TOP_ANGLE)
)
ManeuverRight = Maneuver(
    type_from=ManeuverLeft.type_to,
    type_to=ManeuverLeft.type_from
)
ManeuverLeftUp = Maneuver(
    type_from=(TileType.LEFT_BORDER, TileType.TOP_BORDER, TileType.LEFT_TOP_ANGLE, TileType.LEFT_TOP_DIAGONAL, TileType.RIGHT_TOP_ANGLE),
    type_to=(TileType.RIGHT_BORDER, TileType.BOTTOM_BORDER, TileType.RIGHT_BOTTOM_DIAGONAL, TileType.RIGHT_BOTTOM_ANGLE, TileType.LEFT_BOTTOM_ANGLE)
)
ManeuverRightUp = Maneuver(
    type_from=(TileType.RIGHT_BORDER, TileType.TOP_BORDER, TileType.RIGHT_TOP_DIAGONAL, TileType.RIGHT_TOP_ANGLE, TileType.LEFT_TOP_ANGLE),
    type_to=(TileType.LEFT_BOTTOM_ANGLE, TileType.LEFT_BOTTOM_DIAGONAL, TileType.LEFT_BORDER, TileType.BOTTOM_BORDER, TileType.RIGHT_BOTTOM_ANGLE)
)
ManeuverLeftDown = Maneuver(
    type_from=ManeuverRightUp.type_to,
    type_to=ManeuverRightUp.type_from
)
ManeuverRightDown = Maneuver(
    type_from=ManeuverLeftUp.type_to,
    type_to=ManeuverLeftUp.type_from
)
MANEUVERS = {
    Maneuvers.UP: ManeuverUp,
    Maneuvers.DOWN: ManeuverDown,
    Maneuvers.RIGHT: ManeuverRight,
    Maneuvers.LEFT: ManeuverLeft,
    Maneuvers.RIGHT_UP: ManeuverRightUp,
    Maneuvers.RIGHT_DOWN: ManeuverRightDown,
    Maneuvers.LEFT_UP: ManeuverLeftUp,
    Maneuvers.LEFT_DOWN: ManeuverLeftDown
}


class Synthesizer(object):
    """
    Базовый класс синтезатора программных траекторий для упрощенной машинки
    """

    SUCCESS = 0
    INTEGRATION_FAILED = 1
    COLLISION_HANDLED = 2

    def __init__(self, environment, tile_size, min_pass_distance, x, y, angle):
        assert isinstance(environment, Map)
        self.track = Track(environment, tile_size)
        self.waypoints = [get_tile_index(wp[0], wp[1], tile_size) for wp in environment.waypoints]
        self.initial_state = np.array([x, y, angle])

        m = CLEAR_PATH * np.ones_like(self.track.map)

        # noinspection PyUnresolvedReferences
        m[self.waypoints[0]] = 0
        tile = self.track.map[self.waypoints[0]]
        assert isinstance(tile, Tile)

        # Выбираем индекс следующего тайла исходя из начальной ориентировки машинки
        next_tile_index = INITIAL_WAYS[np.abs(INITIAL_ANGLES-angle).argmin()](tile)

        # noinspection PyUnresolvedReferences
        m[next_tile_index] = 1

        # Строим маршруты до первого waypoint'a
        routes = self.create_route(m, self.waypoints[1])

        # Строим маршруты до всех остальных waypoint'ов
        for waypoint in self.waypoints[2:]:
            new_routes = []
            for route in routes:
                m = CLEAR_PATH * np.ones_like(self.track.map)
                endpoint = route.pop()
                tile = self.track.map[endpoint]
                # noinspection PyUnresolvedReferences
                m[endpoint] = 0
                assert isinstance(tile, Tile)  # fixme debug
                for neighbour in self.__get_ways_from_tile(tile):
                    if neighbour != route[-1]:
                        # noinspection PyUnresolvedReferences
                        m[neighbour] = 1
                r = self.create_route(m, waypoint)
                l = len(route)
                route.extend(r.pop())
                while r:
                    new_routes.append(route[0:l] + r.pop())
            routes.extend(new_routes)

        self.tile_routes, min_route_len = set(), min([len(route) for route in routes])

        for route in routes:

            # Убиваем петли
            # if len(set(route[1:])) != len(route[1:]):
            #     print "Killed route: %s" % route
            #     continue

            if len(route) == min_route_len:
                self.tile_routes.add(tuple(route))

        wp = np.array(environment.waypoints)
        self.routes = [self.create_hardpoints(route) for route in self.tile_routes]

        self.weights, self.turns, self.lengths = [], [], []
        for route in self.routes:
            d, i = cdist(wp, np.array(route)), 0
            while i < len(environment.waypoints):
                # print d[i, :].min(), min_pass_distance
                if d[i, :].min() >= min_pass_distance:
                    route[d[i, :].argmin()] = environment.waypoints[i]
                    d, i = cdist(wp, np.array(route)), 0
                    continue
                i += 1
            self.turns.append(calc_route_weight(route))
            self.lengths.append(calc_route_length(route))
            self.weights.append(self.turns[-1]*self.lengths[-1])

        self.best_route = self.routes[self.weights.index(min(self.weights))]

    def __get_ways_from_tile(self, tile):
        """
        :type tile: Tile
        """
        tile_from_type = int(tile.tile_type)
        assert tile_from_type != TileType.EMPTY, "We are in empty tile %s" % tile
        ways = []
        for maneuver, neighbour in self.track.get_tile_neighbours(tile):
            assert isinstance(neighbour, Tile)
            tile_to_type = int(neighbour.tile_type)
            if tile_to_type == TileType.EMPTY \
                    or tile_from_type in MANEUVERS[maneuver].type_from \
                    or tile_to_type in MANEUVERS[maneuver].type_to:
                continue
            ways.append(tuple(neighbour.index))
        return ways

    def create_route(self, m, endpoint):
        """
        :type m: np.ndarray
        :type endpoint: tuple
        """
        assert isinstance(m, np.ndarray)  # fixme debug
        assert len(endpoint) == 2  # fixme debug

        # Wave propagation
        propagation = 1
        while m[endpoint] == CLEAR_PATH and propagation > 0:
            d = m.max() + 1
            # noinspection PyTypeChecker
            propagation = 0
            # noinspection PyTypeChecker
            for checkpoint in np.argwhere(m == d - 1):
                assert len(checkpoint) == 2  # fixme debug
                tile = self.track.map[checkpoint[0], checkpoint[1]]
                assert isinstance(tile, Tile), "Not a tile: %s (%s)" % (tile, type(tile))
                for neighbour in self.__get_ways_from_tile(tile):
                    if m[neighbour] == CLEAR_PATH:
                        m[neighbour] = d
                        propagation += 1
                # print "Current tile: %s \t propagated: %s" % (tile, propagation)
        assert m[endpoint] != CLEAR_PATH, "No route found to (%s, %s)\n %s" % (endpoint[0], endpoint[1], m)

        # Restore path
        tile = self.track.map[endpoint]
        assert isinstance(tile, Tile)  # fixme debug

        # for neighbour in self.__get_ways_from_tile(tile):
        #     if m[neighbour] == m[endpoint] - 1:
        #         routes.append(self.__back_step(m, [endpoint, tuple(neighbour)]))
        routes = self.__back_step(m, m[endpoint], [[endpoint,]])
        for route in routes:
            route.reverse()
        unique_routes = set()
        for route in routes:
            unique_routes.add(tuple(route))
        return [list(route) for route in unique_routes]

    def __back_step(self, m, d, routes):
        # endpoint = route[-1]
        # assert isinstance(endpoint, tuple)
        # assert len(endpoint) == 2
        # d = m[endpoint] - 1

        assert all(m[route[-1]] == d for route in routes), "Bad back step: d=%s" % d
        d -= 1

        if d == CLEAR_PATH:
            return routes

        new_routes = []

        for route in routes:
            tile = self.track.map[route[-1]]
            assert isinstance(tile, Tile)  # fixme debug
            assert isinstance(route, list)
            # FIXME поправить для перекрестков
            # next_step = None
            for neighbour in self.__get_ways_from_tile(tile):
                if m[neighbour] == d:
                    for new_route in self.__back_step(m, d, [route + [neighbour, ],]):
                        assert isinstance(new_route, list)
                        new_routes.append(new_route)
        return new_routes

    def create_hardpoints(self, route):
        hardpoints = [tuple(self.initial_state[0:2]), ]
        for i in xrange(0, len(route) - 1):
            ci, ni = route[i:i+2]
            ct = self.track.map[ci]
            assert isinstance(ct, Tile)
            if ci[0] + 1 == ni[0] and ci[1] == ni[1]:
                hardpoints.append((ct.middle_x, ct.y1))
            elif ci[0] - 1 == ni[0] and ci[1] == ni[1]:
                hardpoints.append((ct.middle_x, ct.y0))
            elif ci[0] == ni[0] and ci[1] + 1 == ni[1]:
                hardpoints.append((ct.x1, ct.middle_y))
            elif ci[0] == ni[0] and ci[1] - 1 == ni[1]:
                hardpoints.append((ct.x0, ct.middle_y))
            elif ci[0] + 1 == ni[0] and ci[1] + 1 == ni[1]:
                hardpoints.append((ct.x1, ct.y1))
            elif ci[0] + 1 == ni[0] and ci[1] - 1 == ni[1]:
                hardpoints.append((ct.x0, ct.y1))
            elif ci[0] - 1 == ni[0] and ci[1] + 1 == ni[1]:
                hardpoints.append((ct.x1, ct.y0))
            elif ci[0] - 1 == ni[0] and ci[1] - 1 == ni[1]:
                hardpoints.append((ct.x0, ct.y0))
            else:
                raise Exception("Can\'t detect hardpoints for %s, %s" % (ci, ni))
        ct = self.track.map[route[-1]]
        hardpoints.append((ct.middle_x, ct.middle_y))
        return hardpoints

    def create_trajectories(self):
        # assert isinstance(pool, mp.Pool)

        args = []
        for trajectory_cls in (
                # BaseTrajectory,
                BaseSplineTrajectory,
                BaseLinearTrajectory,
                BaseKinematicTrajectory,
                KinematicTrajectory
        ):
            args.append(
                (
                    trajectory_cls,
                    copy(self.track),
                    deque(self.best_route),
                    np.hstack((self.initial_state, 0)) if issubclass(trajectory_cls, KinematicTrajectory)
                    else np.copy(self.initial_state)
                )
            )
        pool = mp.Pool(min([mp.cpu_count()-1, len(args)]))
        trajectories = pool.map(create_trajectory, args)
        pool.close()
        pool.join()
        # trajectories = [create_trajectory(arg) for arg in args]
        return trajectories

    def mpl_draw_routes(self, axis):

        for route in self.routes:
            for i in xrange(len(route)-1):
                axis.plot(route[i][0], route[i][1], "bo", markersize=5.0)
                axis.arrow(
                    route[i][0], route[i][1], 0.25*(route[i+1][0]-route[i][0]), 0.25*(route[i+1][1]-route[i][1]),
                    head_width=0.75, head_length=1.0, fc='k', ec='k'
                )

    def mpl_draw_best_route(self, axis):
        for i in xrange(len(self.best_route)-1):
            axis.plot(self.best_route[i][0], self.best_route[i][1], "ko", markersize=5.0)
            axis.plot(
                [self.best_route[i][0], self.best_route[i+1][0]], [self.best_route[i][1], self.best_route[i+1][1]], "k-"
            )
            axis.arrow(
                self.best_route[i][0], self.best_route[i][1],
                0.5*(self.best_route[i+1][0]-self.best_route[i][0]),
                0.5*(self.best_route[i+1][1]-self.best_route[i][1]),
                head_width=0.75, head_length=1.0, fc='k', ec='k'
            )

    def mpl_draw_graph(self, axis, draw_tile_type=True):
        for i in xrange(self.track.width):
            for j in xrange(self.track.height):
                x = (i+1)*GRAPH_ALIGN
                y = (j+1)*GRAPH_ALIGN
                c = plt.Circle(
                    (x, y), radius=GRAPH_BASE_RADIUS, facecolor='white', edgecolor="black", zorder=3
                )

                axis.add_patch(c)
                tile = self.track.map[j][i]
                assert isinstance(tile, Tile)
                tile_type = int(tile.tile_type)
                text = "%s, %s" % (j, i)
                if draw_tile_type:
                    text += "\n%s" % tile_type
                axis.text(
                    x, y, text, fontsize=GRAPH_FONT_SIZE,
                    color="black", verticalalignment='center', horizontalalignment='center', zorder=5
                )
                if tile_type == TileType.EMPTY:
                    continue

                for nj, ni in self.__get_ways_from_tile(tile):
                    axis.plot([x, (ni+1)*GRAPH_ALIGN], [y, (nj+1)*GRAPH_ALIGN], "k-", linewidth=0.75)
        for j, i in self.waypoints:
            c = plt.Circle(
                ((i+1)*GRAPH_ALIGN, (j+1)*GRAPH_ALIGN),
                radius=GRAPH_BASE_RADIUS, facecolor='blue', edgecolor="none", zorder=6
            )
            axis.add_patch(c)
            axis.text(
                (i+1)*GRAPH_ALIGN, (j+1)*GRAPH_ALIGN, "%s, %s\n%s" % (j, i, self.track.map[j][i].tile_type),
                fontsize=GRAPH_FONT_SIZE, color="white", verticalalignment='center',
                horizontalalignment='center', zorder=6
            )
        # mng = plt.get_current_fig_manager()
        # mng.window.state('zoomed')
        # axis.set_xlim([GRAPH_ALIGN - GRAPH_BASE_RADIUS, self.track.width*GRAPH_ALIGN + GRAPH_BASE_RADIUS])
        axis.axis('off')

    def mpl_draw_graph_routes(self, axis):
        self.mpl_draw_graph(axis, False)

        marked_tiles = []

        for route in self.tile_routes:
            for k in xrange(len(route)-1):
                j, i = route[k]
                x, y = (i+1)*GRAPH_ALIGN, (j+1)*GRAPH_ALIGN

                nj, ni = route[k+1]
                axis.plot([x, (ni+1)*GRAPH_ALIGN], [y, (nj+1)*GRAPH_ALIGN], "g-", linewidth=2.5)

                if route[k] in marked_tiles:
                    continue
                marked_tiles.append(tuple(route[k]))

                color = "blue" if (j, i) in self.waypoints else "green"
                c = plt.Circle(
                    (x, y),
                    radius=GRAPH_BASE_RADIUS, facecolor=color, edgecolor="none", zorder=7
                )
                axis.add_patch(c)
                axis.text(
                    x, y, "%s, %s\n%s" % (j, i, k),
                    fontsize=GRAPH_FONT_SIZE, color="white",
                    verticalalignment='center', horizontalalignment='center', zorder=8
                )


def calc_route_weight(route):
    weight, l = 0, len(route)
    angles = []
    for i in xrange(l-1):
        x, y = route[i + 1]
        x0, y0 = route[i]
        angles.append(float(np.arctan2(y - y0, x - x0)))

    # print list(np.rad2deg(angles))
    for i in xrange(1, len(angles)):
        turn = angles[i] - angles[i-1]
        if turn > np.pi:
            turn -= 2.0*np.pi
        elif turn < -np.pi:
            turn += 2.0*np.pi
        # print i, np.rad2deg(turn)
        weight += abs(turn)
    return weight / l


def calc_route_length(route):
    length = 0.0
    for i in xrange(len(route)-1):
        length += np.hypot(route[i+1][0] - route[i][0], route[i+1][1] - route[i][1])
    return length


def create_trajectory(arguments):

    trajectory_cls, track, hardpoints, initial_state = arguments

    assert issubclass(trajectory_cls, Trajectory), "Bad trajectory type"

    trajectory = trajectory_cls(
        track,
        hardpoints,
        initial_state
    )

    assert isinstance(trajectory, Trajectory)

    trajectory.synthesize()

    return trajectory.get_program_data()


# noinspection PyUnresolvedReferences
class Trajectory(object):
    """
    Расчет программной траектории движения
    """
    controls_size = 2
    obj_cls = BaseObject

    min_valid_disorientation = 0.012
    # assert issubclass(obj_cls, BaseObject), "Bad car type"
    # assert issubclass(obj_cls, BaseCar), "Bad car type"

    # hardpoint_passed_xe = 0.65
    # hardpoint_passed_ye = 1.5
    hardpoint_passed_distance = 1.5

    def __init__(self, track, hardpoints, initial_state):
        self.hardpoints = hardpoints
        self.track = track
        self.car = self.obj_cls(self.__class__.__name__, *initial_state)
        self.data = np.hstack((0.0, self.car.state, np.zeros((self.controls_size,))))

        self.success = True
        self.controls = list(np.zeros((self.controls_size, ), dtype=float))

    @property
    def is_finished(self):
        """
        Флаг, показывающий, что программная траектория полностью и корректно посчитана
        """
        return len(self.hardpoints) == 0 and self.success

    @property
    def is_controls_set(self):
        """
        Флаг, установлены ли управления
        """
        return np.count_nonzero(self.controls) > 0

    @property
    def identificator(self):
        """
        Идентификатор программной траектории
        """
        return self.car.id

    # noinspection PyUnresolvedReferences
    def get_mismatch(self):
        """
        Расчет ошибок в СК, связанной с ОУ
        """
        ax, ay = self.car.direction
        dx, dy = self.hardpoints[0][0] - self.car.x, self.hardpoints[0][1] - self.car.y

        xe = ax * dx + ay * dy
        ye = -ay * dx + ax * dy

        return xe, ye

    def hardpoint_is_passed(self):
        """
        Проверка, пора ли переходить к следующей опорной точке
        """
        # xe, ye = self.get_mismatch()
        # return abs(xe) <= self.hardpoint_passed_xe and abs(ye) <= self.hardpoint_passed_ye
        return np.hypot(
            self.hardpoints[0][0] - self.car.x, self.hardpoints[0][1] - self.car.y
        ) <= self.hardpoint_passed_distance

    def update_controls(self):
        raise NotImplementedError

    def get_desired_speed(self):
        raise NotImplementedError

    def get_program_data(self):
        return ProgramTrajectory(
            self.identificator,
            self.data,
            self.is_finished
        )

    def synthesize(self):
        """
        Непосредственный расчет программной траектории
        """

        assert len(self.hardpoints) > 0

        while self.success:

            # Проверяем расстояние до ближайшей опорной точки
            while len(self.hardpoints) > 0 and self.hardpoint_is_passed():
                self.hardpoints.popleft()
                self.controls = list(np.zeros((self.controls_size, ), dtype=float))

            if not self.hardpoints:
                break

            self.update_controls()

            assert len(self.controls) == self.controls_size, "Bad controls size"
            assert self.is_controls_set, "Controls is not set"
            ang_speed, program_speed = self.get_desired_speed()
            self.data = np.vstack((
                self.data, np.hstack((self.car.integrator.t, self.car.state, program_speed, ang_speed))
            ))

            success = self.car.move(self.car.integrator.t + TIME_SUB_STEP, *self.controls)

            if self.track.collide_object(self.car) or not success:
                if not success:
                    print "Trajectory %s integration failed" % self.car.id
                    return
                self.success = False
                print "Trajectory %s collision in (%s, %s)" % (self.car.id, self.car.x, self.car.y)

        print "%s finished time=%s (success = %s, data size: %s)" % (
            self.car.id, self.data[-1, 0], self.success, self.data.shape
        )


class BaseTrajectory(Trajectory):

    controls_size = 2

    def __init__(self, track, hardpoints, initial_state):
        super(BaseTrajectory, self).__init__(track, hardpoints, initial_state)
        self.data = self.data.reshape((1, len(initial_state)+self.controls_size+1))
        self.success = True

    def get_desired_speed(self):
        pass

    def calculate(self):
        self.data[-1, -2] = KinematicCar.max_speed
        t = self.data[-1, 0]
        while len(self.hardpoints) > 1:
            hp = self.hardpoints.popleft()
            assert len(hp) == 2
            dx, dy = self.hardpoints[0][0]-hp[0], self.hardpoints[0][1]-hp[1]
            pass_time = np.hypot(dx, dy) / KinematicCar.max_speed
            angle = float(np.arctan2(dy, dx))
            # d_angle = normalize_angle(angle - self.data[-1, 3])
            if self.data[-1, 3] <= 0 and angle > 0.25*np.pi:  # FIXME подгон!!!
                angle -= 2.0*np.pi

            d_angle = angle - self.data[-1, 3]
            angular_speed = d_angle/pass_time
            t += pass_time
            d = np.array((t, hp[0], hp[1], angle, KinematicCar.max_speed, angular_speed))
            # print self.data.shape, d.shape
            self.data = np.vstack((
                self.data,
                d
            ))

    def synthesize(self):
        self.calculate()
        print "%s finished time=%s (success = %s, data size: %s)" % (
            self.car.id, self.data[-1, 0], self.success, self.data.shape
        )

    def update_controls(self):
        pass

    def get_program_data(self):
        return BaseProgramTrajectory(
            self.identificator,
            self.data,
            self.is_finished
        )


class BaseSplineTrajectory(BaseTrajectory):
    kind = 'cubic'
    n = 20000
    dt = 0.05

    def update_controls(self):
        pass

    def get_desired_speed(self):
        pass

    def get_program_data(self):
        return ProgramTrajectory(
            self.identificator,
            self.data,
            self.is_finished
        )

    def calculate(self):
        super(BaseSplineTrajectory, self).calculate()
        t = self.data[:, 0]
        t_i = np.linspace(self.data[0, 0], self.data[-1, 0], num=self.n, endpoint=True)

        t_angular, a_speed = [], []
        for i in xrange(len(t) - 1):
            t_angular.append(t[i])
            t_angular.append(t[i+1] - self.dt)
            a_speed.append(self.data[i, 5])
            a_speed.append(self.data[i, 5])
        t_angular.append(t[-1])
        a_speed.append(self.data[-1, 5])

        x = interp1d(t, self.data[:, 1], kind=self.kind)(t_i)
        y = interp1d(t, self.data[:, 2], kind=self.kind)(t_i)
        angle = interp1d(t, self.data[:, 3], kind=self.kind)(t_i)
        speed = interp1d(t, self.data[:, 4], kind=self.kind)(t_i)
        angular_speed = interp1d(t_angular, a_speed, kind='linear')(t_i)

        # for i in xrange(len(angle)):
        #     angle[i] = normalize_angle(angle[i])

        self.data = np.column_stack((t_i, x, y, angle, speed, angular_speed))


class BaseLinearTrajectory(BaseSplineTrajectory):
    kind = 'linear'


class BasePolynomialTrajectory(BaseTrajectory):

    n = 20000

    def update_controls(self):
        pass

    def get_desired_speed(self):
        pass

    def get_program_data(self):
        return ProgramTrajectory(
            self.identificator,
            self.data,
            self.is_finished
        )

    def calculate(self):
        super(BasePolynomialTrajectory, self).calculate()
        t = self.data[:, 0]
        t_i = np.linspace(self.data[0, 0], self.data[-1, 0], num=self.n, endpoint=True)
        deg = len(t) - 1
        x = np.polyval(np.polyfit(t, self.data[:, 1], deg), t_i)
        y = np.polyval(np.polyfit(t, self.data[:, 2], deg), t_i)
        angle = np.polyval(np.polyfit(t, self.data[:, 3], deg), t_i)
        speed = np.polyval(np.polyfit(t, self.data[:, 4], deg), t_i)
        angular_speed = np.polyval(np.polyfit(t, self.data[:, 5], deg), t_i)

        self.data = np.column_stack((t_i, x, y, angle, speed, angular_speed))


# noinspection PyUnresolvedReferences
class KinematicTrajectory(Trajectory):

    K_v = 0.0
    obj_cls = KinematicCar

    # hardpoint_passed_distance = 2.0

    def update_controls(self):
        """
        Вычисление управлений
        """

        # Разориентировка относительно ближайшей опорной точки
        disorientation = self.car.get_angle_to(*self.hardpoints[0])

        # e_angle = abs(disorientation)
        #
        # if self.is_controls_set:
        # if e_angle < self.min_valid_disorientation:
        #     self.controls = [1.0, 0.0]
        #     return

        if abs(disorientation) >= KinematicCar.max_wheel_turn:
            self.controls[1] = 1.0 if disorientation > 0 else -1.0
        else:
            self.controls[1] = disorientation / KinematicCar.max_wheel_turn
        self.controls[0] = 1.0 - self.K_v*abs(self.controls[1])

    def get_desired_speed(self):
        # Расчет желаемой угловой скорости
        ang_speed = float(np.tan(self.car.wheel_turn_interpolation(self.controls[1]))) / self.car.width
        ang_speed *= self.controls[0]
        return ang_speed, self.car.speed_interpolation(self.controls[0])

    def get_program_data(self):
        return KinematicProgramTrajectory(
            self.identificator,
            self.data,
            self.is_finished
        )


class BrakeTrajectory(KinematicTrajectory):

    K_v = 0.5

    def get_desired_speed(self):
        return super(BrakeTrajectory, self).get_desired_speed()

    def update_controls(self):
        super(BrakeTrajectory, self).update_controls()


class BaseKinematicTrajectory(Trajectory):
    
    obj_cls = MaterialPoint
    K_l = 0.75

    def __init__(self, track, hardpoints, initial_state):
        super(BaseKinematicTrajectory, self).__init__(track, hardpoints, initial_state)
        # self.last_hardpoint = tuple(hardpoints[0])

    # def hardpoint_is_passed(self):
    #     if super(BaseKinematicTrajectory, self).hardpoint_is_passed():
    #         self.last_hardpoint = tuple(self.hardpoints[0])
    #         return True
    #     return False

    # noinspection PyUnresolvedReferences
    def update_controls(self):

        # Разориентировка относительно ближайшей опорной точки
        disorientation = self.car.get_angle_to(*self.hardpoints[0])

        # e_angle = abs(disorientation)

        if self.is_controls_set:
            # if e_angle < self.min_valid_disorientation:
            #     self.controls = [MaterialPoint.max_speed, 0.0]
            return

        l = self.K_l*np.hypot(self.hardpoints[0][0] - self.car.x, self.hardpoints[0][1] - self.car.y)
        self.controls[0] = MaterialPoint.max_speed
        self.controls[1] = -MaterialPoint.max_speed * disorientation / l

    def get_desired_speed(self):
        return self.controls[1], self.controls[0]


class BaseKinematicTrajectory2(BaseKinematicTrajectory):

    def __init__(self, track, hardpoints, initial_state):
        super(BaseKinematicTrajectory, self).__init__(track, hardpoints, initial_state)
        self.last_hardpoint = tuple(hardpoints[0])

    def get_desired_speed(self):
        return super(BaseKinematicTrajectory2, self).get_desired_speed()

    def hardpoint_is_passed(self):
        if super(BaseKinematicTrajectory, self).hardpoint_is_passed():
            self.last_hardpoint = tuple(self.hardpoints[0])
            return True
        return False

    # noinspection PyUnresolvedReferences
    def update_controls(self):

        # Разориентировка относительно ближайшей опорной точки
        disorientation = self.car.get_angle_to(*self.hardpoints[0])

        e_angle = abs(disorientation)

        if self.is_controls_set:
            if e_angle < self.min_valid_disorientation:
                self.controls = [MaterialPoint.max_speed, 0.0]
            return

        l = np.hypot(self.hardpoints[0][0] - self.last_hardpoint[0], self.hardpoints[0][1] - self.last_hardpoint[1])
        self.controls[0] = MaterialPoint.max_speed
        self.controls[1] = -MaterialPoint.max_speed * disorientation / l


class ProgramTrajectory(object):

    def __init__(self, trajectory_id, data, is_finished):
        self.identificator = trajectory_id
        self.data = data
        self.is_finished = is_finished

    @property
    def time(self):
        return self.data[:, 0]

    @property
    def x(self):
        return self.data[:, 1]

    @property
    def y(self):
        return self.data[:, 2]

    @property
    def angle(self):
        return self.data[:, 3]

    @property
    def speed(self):
        return self.data[:, 4]

    @property
    def angular_speed(self):
        return self.data[:, 5]

    @property
    def duration(self):
        """
        Общая продолжительность программной траектории
        """
        return self.data[-1, 0]


class BaseProgramTrajectory(ProgramTrajectory):
    pass


class KinematicProgramTrajectory(ProgramTrajectory):

    @property
    def wheel_turn(self):
        return self.data[:, 4]

    @property
    def speed(self):
        return self.data[:, 5]

    @property
    def angular_speed(self):
        return self.data[:, 6]


if __name__ == '__main__':
    from src.kernel.track.maps import Map02 as M
    from src.kernel.track.maps import WAYPOINTS_MAP02_01 as WP

    TILE_SIZE = 5.0
    MIN_PASS_DISTANCE = 0.5 * TILE_SIZE

    fig = plt.figure()
    ax = fig.add_subplot(111)

    m = M(WP, MIN_PASS_DISTANCE)
    s = Synthesizer(m, TILE_SIZE, MIN_PASS_DISTANCE, WP[0][0], WP[0][1], 0.5*np.pi)
    print "Created %s routes" % len(s.routes)

    s.track.mpl_draw(ax, indices=True, tile_type=True, tile_grid=True, disable_axis=False)
    m.mpl_draw(ax, waypoints_area=True, ticks=True)

    # Plot all routes
    fig = plt.figure()
    ax = fig.add_subplot(111)
    s.mpl_draw_routes(ax)
    s.track.mpl_draw(ax, tiles=False, tile_grid=True)
    m.mpl_draw(ax, waypoints_area=True, labels=False, ticks=True)

    # Plot best route
    fig = plt.figure()
    ax = fig.add_subplot(111)
    s.track.mpl_draw(ax, tiles=False, tile_grid=True)
    m.mpl_draw(ax, waypoints_area=True, labels=False, ticks=True)
    s.mpl_draw_best_route(ax)

    # Plot graph
    fig = plt.figure()
    ax = fig.add_subplot(111)
    s.mpl_draw_graph(ax)

    # Plot routes on graph
    fig = plt.figure()
    ax = fig.add_subplot(111)
    s.mpl_draw_graph_routes(ax)

    plt.show()