# coding=utf-8
import numpy as np
from scipy.spatial.distance import cdist
from scipy.integrate import simps

from src.kernel.objects.car import RealCar
from src.utils import normalize_angle, saturate
from src.synthesizer import ProgramTrajectory


def select_trajectory(trajectories):
    min_program_time, active_trajectory_index = np.inf, None

    for i in xrange(len(trajectories)):
        trajectory = trajectories[i]
        assert isinstance(trajectory, ProgramTrajectory), "not Trajectory"

        if trajectory.duration < min_program_time and trajectory.is_finished:
            active_trajectory_index, min_program_time = i, trajectory.duration

    assert active_trajectory_index is not None, "Program trajectory not selected"
    return active_trajectory_index


class Controller(object):

    K_angle = 3.25
    K_coord = 1.75
    K_p = 15.0
    K_i = 0.0

    min_e_coord = 0.20
    min_e_angle = 0.03

    def __init__(self, trajectory, k_v=0.0):
        assert isinstance(trajectory, ProgramTrajectory)
        self.trajectory = trajectory
        self.K_v = k_v
        self.data = np.zeros((1, 8), dtype=np.float)
        self.d_omega = np.zeros((1,))

    @property
    def program_time(self):
        return self.trajectory.time

    @property
    def x(self):
        return self.trajectory.x

    @property
    def y(self):
        return self.trajectory.y

    @property
    def angle(self):
        return self.trajectory.angle

    @property
    def speed(self):
        return self.trajectory.speed

    @property
    def angular_speed(self):
        return self.trajectory.angular_speed

    @property
    def time(self):
        return self.data[1:, 0]

    @property
    def duration(self):
        return self.time[-1]

    @property
    def e_angle(self):
        return self.data[1:, 1]

    @property
    def e_coord(self):
        return self.data[1:, 2]

    @property
    def speed_d(self):
        return self.data[1:, 3]

    @property
    def angular_speed_d(self):
        return self.data[1:, 4]

    @property
    def omega_d(self):
        return self.data[1:, 5]

    @property
    def wheel_turn_d(self):
        return self.data[1:, 6]

    @property
    def u(self):
        return self.data[1:, 7]

    def __unpack_trajectory_data(self, index):
        return self.trajectory.x[index], \
               self.trajectory.y[index], \
               self.trajectory.angle[index], \
               self.trajectory.speed[index], \
               self.trajectory.angular_speed[index]

    def get_controls(self, t, x, y, angle, omega, *args):
        position = np.array([(x, y), ], dtype=float)

        i = cdist(position, self.trajectory.data[1:, 1:3]).argmin()

        x_r, y_r, angle_r, speed_r, angular_speed_r = self.__unpack_trajectory_data(i + 1)

        e_angle = normalize_angle(angle_r - angle)
        # e_angle = angle_r - angle
        e_coord = (y_r - y)*np.cos(angle_r) - (x_r - x)*np.sin(angle_r)

        if abs(e_angle) <= self.min_e_angle:
            e_angle = 0.0

        if abs(e_coord) <= self.min_e_coord:
            e_coord = 0.0

        speed_d = speed_r * (1.0 - self.K_v*abs(e_angle)/np.pi)
        angular_speed_d = angular_speed_r + self.K_angle*e_angle + self.K_coord*e_coord

        wheel_turn = saturate(np.arctan(angular_speed_d*RealCar.width/speed_d), RealCar.max_wheel_turn)

        omega_d = speed_d/(RealCar.wheel_rear_radius*np.sqrt(1 + (RealCar.arm_rear*np.tan(wheel_turn)/RealCar.width)**2))

        self.d_omega = np.append(self.d_omega, [omega - omega_d, ])
        x = np.hstack((self.data[:, 0], t))
        assert len(self.d_omega) == len(x)
        ku = -self.K_p*(self.d_omega[-1])
        if len(self.d_omega) > 100 and self.K_i > 0.0:
            ku -= self.K_i*simps(self.d_omega[-100:])

        u = saturate(ku + RealCar.h*omega, RealCar.M_max)

        self.data = np.vstack((
            self.data,
            [t, e_angle, e_coord, speed_d, angular_speed_d, omega_d, wheel_turn, u]
        ))

        return u, wheel_turn
