# coding=utf-8
from collections import OrderedDict


TILE_POINTS = ("lt", "lm", "lb", "mt", "mm", "mb", "rt", "rm", "rb")


class TileType(object):
    EMPTY = 13
    FREE = 0
    LEFT_TOP_DIAGONAL = 1
    RIGHT_TOP_DIAGONAL = 2
    RIGHT_BOTTOM_DIAGONAL = 3
    LEFT_BOTTOM_DIAGONAL = 4
    LEFT_BORDER = 5
    TOP_BORDER = 6
    RIGHT_BORDER = 7
    BOTTOM_BORDER = 8
    LEFT_TOP_ANGLE = 9
    RIGHT_TOP_ANGLE = 10
    RIGHT_BOTTOM_ANGLE = 11
    LEFT_BOTTOM_ANGLE = 12

TILE_TYPES = OrderedDict()
TILE_TYPES[TileType.FREE] = ()
TILE_TYPES[TileType.RIGHT_TOP_DIAGONAL] = ("rt", )
TILE_TYPES[TileType.RIGHT_BOTTOM_DIAGONAL] = ("rb", )
TILE_TYPES[TileType.LEFT_BOTTOM_DIAGONAL] = ("lb", )
TILE_TYPES[TileType.LEFT_TOP_DIAGONAL] = ("lt", )
TILE_TYPES[TileType.TOP_BORDER] = ("lt", "mt", "rt")
TILE_TYPES[TileType.LEFT_BORDER] = ("lm", "lt", "lb")
TILE_TYPES[TileType.BOTTOM_BORDER] = ("lb", "mb", "rb")
TILE_TYPES[TileType.RIGHT_BORDER] = ("rt", "rm", "rb")
TILE_TYPES[TileType.LEFT_TOP_ANGLE] = ("lm", "lt", "lb", "mt", "rt")
TILE_TYPES[TileType.RIGHT_TOP_ANGLE] = ("lt", "mt", "rt", "rm", "rb")
TILE_TYPES[TileType.RIGHT_BOTTOM_ANGLE] = ("rt", "rm", "rb", "mb", "lb")
TILE_TYPES[TileType.LEFT_BOTTOM_ANGLE] = ("rb", "mb", "lb", "lm", "lt")
TILE_TYPES[TileType.EMPTY] = TILE_POINTS

TILE_MATCH_TYPE = dict(TILE_TYPES)
TILE_MATCH_TYPE[TileType.RIGHT_TOP_DIAGONAL] = ("rt", "mt", "rm")
TILE_MATCH_TYPE[TileType.RIGHT_BOTTOM_DIAGONAL] = ("rb", "rm", "mb")
TILE_MATCH_TYPE[TileType.LEFT_BOTTOM_DIAGONAL] = ("lb", "lm", "mb")
TILE_MATCH_TYPE[TileType.LEFT_TOP_DIAGONAL] = ("lt", "lm", "mt")


class Tile(object):

    def __init__(self, i, j, tile_size):
        """
        :type i: int
        :type j: int
        """

        self.index = (i, j)

        self.x0 = tile_size * j
        self.y0 = tile_size * i
        self.x1 = tile_size * j + tile_size
        self.y1 = tile_size * i + tile_size

        self.middle_x = self.x0 + 0.5 * (self.x1 - self.x0)
        self.middle_y = self.y0 + 0.5 * (self.y1 - self.y0)

        self.lt = (self.x0, self.y1, True)
        self.mt = (self.middle_x, self.y1, True)
        self.rt = (self.x1, self.y1, True)

        self.lm = (self.x0, self.middle_y, True)
        self.mm = (self.middle_x, self.middle_y, True)
        self.rm = (self.x1, self.middle_y, True)

        self.lb = (self.x0, self.y0, True)
        self.mb = (self.middle_x, self.y0, True)
        self.rb = (self.x1, self.y0, True)

    @property
    def points(self):
        return (
            self.lt, self.lm, self.lb,
            self.mt, self.mm, self.mb,
            self.rt, self.rm, self.rb,
        )

    @property
    def zipped(self):
        return zip(*self.points)

    @property
    def tile_type(self):

        for possible_type, missed_points in TILE_TYPES.items():
            success = True
            for pk in TILE_POINTS:
                if not self.__dict__[pk][2] and pk not in missed_points:
                    success = False
                    break
            if success:
                return possible_type
        return TileType.EMPTY

    def check_matching_type(self):
        tile_type = int(self.tile_type)
        for point in TILE_POINTS:
            if point in TILE_TYPES[tile_type]:
                x, y, _ = self.__dict__[point]
                self.__dict__[point] = (x, y, False)

    def __contains__(self, point):

        assert len(point) == 2  # fixme debug

        return self.x0 <= point[0] <= self.x1 and self.y0 <= point[1] <= self.y1

    def __repr__(self):
        return "Tile(%s, %s)" % (self.index[0], self.index[1])

    def mpl_draw(self, axis):

        for point in self.points:
            x, y, is_free = point
            if is_free:
                axis.plot(x, y, "bo", markersize=3.0)
            else:
                axis.plot(x, y, "kx", markersize=8.0, linewidth=4.0)

    def mpl_grid(self, axis, color="k", line_width=1.0):
        line_style = "%s--" % color

        axis.plot([self.x0, self.x1], [self.y0, self.y0], line_style, linewidth=line_width)
        axis.plot([self.x0, self.x1], [self.y1, self.y1], line_style, linewidth=line_width)
        axis.plot([self.x0, self.x0], [self.y0, self.y1], line_style, linewidth=line_width)
        axis.plot([self.x1, self.x1], [self.y0, self.y1], line_style, linewidth=line_width)