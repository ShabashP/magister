# coding=utf-8
import numpy as np
import matplotlib.pyplot as plt
from src.kernel.objects.barrier import *
from src.kernel.objects.bases import BaseObject


__all__ = (
    "Map",
    "Map01",
    "Map02",
    "WAYPOINTS_MAP01_01",
    "WAYPOINTS_MAP01_02",
    "WAYPOINTS_MAP02_01",
)


class Map(object):
    width = 0
    height = 0
    barriers = ()

    def __init__(self, waypoints, min_pass_distance):
        assert all([isinstance(b, Barrier) for b in self.barriers])
        assert all([isinstance(m, int) and m > 0 for m in (self.width, self.height)])
        assert all([len(wp) == 2 for wp in waypoints])
        assert all([not b.collide(*wp) for b in self.barriers for wp in waypoints])

        self.min_pass_distance = min_pass_distance
        self.waypoints = waypoints
        self.__area = np.array([
            (0, 0), (0, self.height), (self.width, self.height), (self.width, 0), (0, 0)
        ])

    def __contains__(self, point):

        assert len(point) == 2  # fixme debug

        return 0 < point[0] < self.width and 0 < point[1] < self.height

    def collide_object(self, obj):
        """
        Определяет коллизии объекта с элементами трассы

        :type obj: BaseObject
        :rtype: bool
        """

        for point in obj.get_borders():
            if self.collide_point(point):
                return True
        return False

    def collide_point(self, point):
        assert len(point) == 2  # fixme debug
        return (point not in self) or any([b.collide(*point) for b in self.barriers])
        # if point not in self:
        #     return True
        # for i in xrange(len(self.barriers)):
        #     if self.barriers[i].collide(*point):
        #         print "Collision with barrier %s (point: %s)" % (i, point)
        #         return True
        # return False

    def barrier_distance(self, x, y):
        return min(
            [b.distance(x, y) for b in self.barriers] + [x, y, abs(self.height-y), abs(self.width-x)]
        )

    def mpl_draw(self, axis, waypoints_area=True, waypoints_order=True, barriers=True, labels=True, ticks=False):

        for i in xrange(len(self.barriers)):
            self.barriers[i].plot_mpl(axis)

            if barriers and i < len(self.barriers)-1:
                axis.text(
                    self.barriers[i].x0, self.barriers[i].y0, r"$B_{%s}$" % i,
                    fontsize=16, color="white", verticalalignment='center', horizontalalignment='center'
                )

        for i in xrange(len(self.waypoints)-1):
            if waypoints_area:
                c = plt.Circle(
                    self.waypoints[i], radius=self.min_pass_distance, facecolor='blue', edgecolor="none", alpha=0.4
                )
                axis.add_patch(c)
            if waypoints_order:
                axis.text(
                    self.waypoints[i][0], self.waypoints[i][1], str(i),
                    fontsize=12, color="blue", verticalalignment='center', horizontalalignment='center'
                )

        axis.plot(self.__area[:, 0], self.__area[:, 1], "g-", linewidth=4.0)
        if labels:
            axis.set_xlabel(r"W")
            axis.set_ylabel(r"H")

        if not ticks:
            axis.xaxis.set_ticklabels([])
            axis.yaxis.set_ticklabels([])
        axis.set_xlim([0, self.width])
        axis.set_ylim([0, self.height])

WAYPOINTS_MAP01_01 = [
    (2.5, 7.5), (12.5, 25.0), (27.5, 52.5), (47.5, 45.0), (27.5, 7.5), (2.5, 7.5)
]


WAYPOINTS_MAP01_02 = [
    (12.5, 17.5), (27.5, 42.5), (47.5, 45.0), (32.5, 22.5), (50.0, 6.0), (12.5, 17.5)
]


class Map01(Map):

    width = 55
    height = 55

    barriers = (
        create_barrier(23.0, 17.5, BARRIER_TYPE_03_POINTS, scale=6.0, angle=-0.78*np.pi),  # 0
        create_barrier(2.55, 28.75, BARRIER_TYPE_05_POINTS, scale=16.0, angle=0.0*np.pi),  # 1
        create_barrier(19.0, 42.0, BARRIER_TYPE_06_POINTS, scale=3.55, angle=-0.23*np.pi),  # 2
        create_barrier(38.5, 42.0, BARRIER_TYPE_01_POINTS, scale=2.75, angle=0.83*np.pi),  # 3
        create_barrier(48.0, 53.5, BARRIER_TYPE_05_POINTS, scale=6.75, angle=-0.49*np.pi),  # 4
        create_barrier(53.5, 42.0, BARRIER_TYPE_05_POINTS, scale=7.75, angle=1.0*np.pi),  # 5
        create_barrier(43.25, 26.0, BARRIER_TYPE_01_POINTS, scale=3.05, angle=-0.84*np.pi),  # 6
        create_barrier(33.0, 12.0, BARRIER_TYPE_02_POINTS, scale=5.75, angle=0.82*np.pi),  # 7
        create_barrier(50.0, 10.0, BARRIER_TYPE_02_POINTS, scale=6.5, angle=0.12*np.pi),  # 8
        create_barrier(42.0, 1.25, BARRIER_TYPE_05_POINTS, scale=5.5, angle=0.5*np.pi),  # 9
        create_barrier(12.0, 8.75, BARRIER_TYPE_05_POINTS, scale=5.25, angle=0.52*np.pi),  # 10
    )


WAYPOINTS_MAP02_01 = [
    (2.5, 7.5), (12.5, 25.0), (27.5, 52.5), (57.5, 32.5), (52.5, 17.5), (32.5, 25.0), (47.5, 7.5), (30.0, 7.5), (2.5, 7.5)
]


class Map02(Map01):
    width = 60

    barriers = (
        create_barrier(23.0, 17.5, BARRIER_TYPE_03_POINTS, scale=6.0, angle=-0.78*np.pi),  # 0
        create_barrier(2.55, 28.75, BARRIER_TYPE_05_POINTS, scale=16.0, angle=0.0*np.pi),  # 1
        create_barrier(19.0, 42.0, BARRIER_TYPE_06_POINTS, scale=3.55, angle=-0.23*np.pi),  # 2
        create_barrier(39.0, 42.0, BARRIER_TYPE_08_POINTS, scale=2.6, angle=0.83*np.pi),  # 3
        create_barrier(48.0, 53.5, BARRIER_TYPE_05_POINTS, scale=7.75, angle=-0.49*np.pi),  # 4
        create_barrier(51.5, 40.0, BARRIER_TYPE_05_POINTS, scale=4.5, angle=1.0*np.pi),  # 5
        create_barrier(51.5, 24.5, BARRIER_TYPE_05_POINTS, scale=4.5, angle=1.0*np.pi),  # 6
        create_barrier(57.75, 11.5, BARRIER_TYPE_02_POINTS, scale=5.25, angle=0.82*np.pi),  # 7
        create_barrier(40.0, 19.0, BARRIER_TYPE_02_POINTS, scale=6.25, angle=0.60*np.pi),  # 8
        create_barrier(40.0, 7.95, BARRIER_TYPE_02_POINTS, scale=5.05, angle=0.82*np.pi),  # 9
        create_barrier(12.0, 8.75, BARRIER_TYPE_05_POINTS, scale=5.25, angle=0.52*np.pi),  # 10
    )

if __name__ == '__main__':

    fig = plt.figure()
    ax = fig.add_subplot(111)

    m = Map02(WAYPOINTS_MAP02_01, 2.5)
    m.mpl_draw(ax, ticks=True)

    plt.show()