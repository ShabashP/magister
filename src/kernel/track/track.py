# coding=utf-8
import numpy as np
import matplotlib.pyplot as plt
from math import floor
from tiles import Tile, TILE_POINTS
from src.kernel.objects.bases import BaseObject
from src.kernel.track.maps import Map

SAFE_DISTANCE = 1.5


# Возможные перемещения между тайлами
# noinspection PyClassHasNoInit
class Maneuvers:
    UP = 1  #
    DOWN = 2  #
    LEFT = 3  #
    RIGHT = 4  #
    RIGHT_UP = 5  #
    RIGHT_DOWN = 6  #
    LEFT_UP = 7  #
    LEFT_DOWN = 8  #


def get_tile_index(x, y, tile_size):
    """
    Получить индекс (i, j) тайла по координатам

    :type x: float
    :type y: float
    :type tile_size: int
    """
    tile_x = int(floor(y/tile_size))
    tile_y = int(floor(x/tile_size))
    return tile_x if tile_x >= 0 else 0, tile_y if tile_y >= 0 else 0


class Track(object):

    def __init__(self, environment, tile_size, safe_distance=SAFE_DISTANCE):
        """
        :type environment: Map
        :type tile_size: int or float
        :type safe_distance: float
        """

        assert environment.width % tile_size == 0, "Bad grid size (width)"
        assert environment.height % tile_size == 0, "Bad grid size (height)"

        self.height = int(environment.height / tile_size)
        self.width = int(environment.width / tile_size)

        self.map = np.empty((self.height, self.width), dtype=Tile)

        for i in xrange(0, self.height):
            for j in xrange(0, self.width):
                tile = Tile(i, j, tile_size)

                for point_name in TILE_POINTS:
                    p_x, p_y, p_is_free = getattr(tile, point_name)
                    if not environment.collide_point((p_x, p_y)) and environment.barrier_distance(p_x, p_y) >= safe_distance:
                        setattr(tile, point_name, (p_x, p_y, True))
                    else:
                        setattr(tile, point_name, (p_x, p_y, False))
                # tile.check_matching_type()
                self.map[i, j] = tile

        self.tile_size = tile_size
        self.environment = environment

    @property
    def font_size(self):
        if self.map.size > 200:
            return 8.0
        return 12.0

    def collide_object(self, obj):
        """
        Определяет коллизии объекта с элементами трассы

        :type obj: BaseObject
        :rtype: bool
        """
        return self.environment.collide_object(obj)

    def get_tile(self, x, y):
        i, j = get_tile_index(x, y, self.tile_size)
        assert 0 <= i < self.height, "Row index out of track: %s (track height: %s)" % (i, self.height)
        assert 0 <= j < self.width, "Column index out of track: %s (track height: %s)" % (j, self.width)
        return self.map[i][j]

    def get_tile_neighbours(self, tile):
        assert isinstance(tile, Tile)
        neighbours = []
        i, j = tile.index
        gh, lh = i < self.height - 1, i > 0
        gw, lw = j < self.width - 1, j > 0

        if gh:
            neighbours.append((Maneuvers.UP, self.map[i+1][j]))
        if lh:
            neighbours.append((Maneuvers.DOWN, self.map[i-1][j]))
        if gw:
            neighbours.append((Maneuvers.RIGHT, self.map[i][j+1]))
        if lw:
            neighbours.append((Maneuvers.LEFT, self.map[i][j-1]))
        if gh and gw:
            neighbours.append((Maneuvers.RIGHT_UP, self.map[i+1][j+1]))
        if gh and lw:
            neighbours.append((Maneuvers.LEFT_UP, self.map[i+1][j-1]))
        if lh and gw:
            neighbours.append((Maneuvers.RIGHT_DOWN, self.map[i-1][j+1]))
        if lh and lw:
            neighbours.append((Maneuvers.LEFT_DOWN, self.map[i-1][j-1]))
        return neighbours

    def mpl_draw(self, axis, tiles=True, tile_grid=False, indices=False, tile_type=False, disable_axis=False):
        for i in xrange(len(self.map)):
            for j in xrange(len(self.map[i])):
                tile = self.map[i][j]
                assert isinstance(tile, Tile), "%s is not Tile" % type(tile)  # fixme debug

                if tiles:  # рисуем тайлы (точки)
                    tile.mpl_draw(axis)

                if tile_grid:  # рисуем сетку тайлов
                    tile.mpl_grid(axis)

                if tile_type:  # подписываем тип тайла
                    axis.text(
                        tile.middle_x + 0.25*self.tile_size,
                        tile.middle_y - 0.25*self.tile_size,
                        "%s" % tile.tile_type,
                        color='black',
                        verticalalignment='center',
                        horizontalalignment='center',
                        fontsize=self.font_size
                    )

                if indices:  # подписываем индекс тайла
                    axis.text(
                        tile.middle_x,
                        tile.middle_y + 0.25*self.tile_size,
                        "(%s, %s)" % (tile.index[0], tile.index[1]),
                        color='black',
                        verticalalignment='center',
                        horizontalalignment='center',
                        fontsize=self.font_size
                    )

        if disable_axis:  # отключаем легенду по осям
            axis.set_xticklabels([])
            axis.set_yticklabels([])


def mpl_plot_waypoints(axis, track, waypoints):

    for ind in xrange(len(waypoints)-1):
        i, j = waypoints[ind]
        tile = track.map[i][j]
        axis.text(tile.middle_x, tile.middle_y, "%s\n(%s, %s)" % (ind, i, j), fontsize=16,
                  color="blue", verticalalignment='center', horizontalalignment='center')


if __name__ == '__main__':

    from src.kernel.track.maps import Map02 as M
    from src.kernel.track.maps import WAYPOINTS_MAP02_01 as WP

    TILE_SIZE = 5

    fig = plt.figure()
    ax = fig.add_subplot(111)

    m = M(WP, 2.5)

    t = Track(m, TILE_SIZE)
    t.mpl_draw(ax, indices=False, tile_type=True, tile_grid=True, disable_axis=False)

    m.mpl_draw(ax, waypoints_area=True, ticks=True)

    plt.show()