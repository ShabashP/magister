# coding=utf-8
import numpy as np
from scipy.constants import g
from scipy.integrate import ode
from scipy.interpolate import interp1d
from src.kernel.objects.bases import BaseObject
from src.kernel.friction import TrackFrictionApproximation
from src.kernel.objects.static_objects import RectangularObject


TOLERANCE = 0.005


# noinspection PyAbstractClass
class BaseCar(object):
    """
    Базовая модель машинки
    """

    # Габариты кузова
    height = 0.5
    width = 1.0

    # Ограничения
    min_speed = 0.05
    max_speed = 1.0  # максимальная скорость
    max_wheel_turn = 0.558  # максимальный угол поворота колес (30 градусов)

    # Перевод относительных значений управлений в абсолютные
    wheel_turn_interpolation = interp1d([-1.0, 1.0], [max_wheel_turn, -max_wheel_turn])
    speed_interpolation = interp1d([-1.0, 1.0], [-max_speed, max_speed])

    # Ограничение на изменение управлений
    max_wheel_turn_change = 0.4

    def __init__(self):

        # Устанавливаем интегратор
        self.integrator = ode(self._process).set_integrator("dopri5", max_step=10e-4)

    def _process(self, t, state, controls):
        """
        Интегрируемая функция
        :param t: текущее время
        :param state: текущее значение вектора состояния

        :return: изменение вектора состояния
        """
        raise NotImplementedError

    def move(self, t_max, *args, **kwargs):
        """

        :param t_max: время, до которого интегрировать
        :param args: параметры
        :param kwargs: параметры

        :rtype: bool
        :return: флаг об успешности интегрирования
        """
        raise NotImplementedError


# noinspection PyAbstractClass
class KinematicCar(RectangularObject, BaseCar):

    _state_props = (
        "x",
        "y",
        "angle"
        "wheel_turn"
    )

    # noinspection PyCallByClass
    def __init__(self, identifier, *initial_state, **kwargs):
        BaseCar.__init__(self)
        BaseObject.__init__(self, identifier, *initial_state)

        self.integrator.set_initial_value(self.state, kwargs.get("time", 0))

    def _process(self, t, state, controls):

        # Желаемые значения управлений
        speed_d, wt_d = controls

        d_dt = np.zeros_like(state)

        d_dt[0] = speed_d * np.cos(state[2])
        d_dt[1] = speed_d * np.sin(state[2])
        d_dt[2] = speed_d * np.tan(state[3]) / self.width

        # Расчет изменения поворота колес
        dwt = wt_d - state[3]  # Нужный угол поворота колес
        d_dt[3] = 0.0 if abs(dwt) < TOLERANCE else self.max_wheel_turn_change * np.sign(dwt)

        return d_dt

    def move(self, t_max, *args, **kwargs):
        speed, wheel_turn = args
        assert -1.0 <= wheel_turn <= 1.0, "Bad relative wheel_turn: %s" % wheel_turn  # fixme debug
        assert -1.0 <= speed <= 1.0, "Bad relative speed %s" % speed  # fixme debug
        assert self.integrator.t < t_max, "Integrator time mismatch: %s >= %s" % (self.integrator.t, t_max)

        self.integrator.set_f_params(
            (
                float(self.speed_interpolation(speed)),
                float(self.wheel_turn_interpolation(wheel_turn))
            )
        )

        self.state = self.integrator.integrate(t_max)

        return self.integrator.successful()


class RealCar(RectangularObject, BaseCar):

    _state_props = (
        "x",                 # 0
        "y",                 # 1
        "angle",             # 2
        "speed_lengthwise",  # 3
        "speed_crosswise",   # 4
        "angular_speed",     # 5
        "omega_r",           # 6
        "omega_f",           # 7
        "torque",            # 8
        "wheel_turn",        # 9
    )

    # Масса
    mass = 125.0
    inverted_mass = 1.0 / mass

    # Нормальная составляющая реакции колес
    # (полагаем, что на каждое колесо приходится четверть массы)
    normal_reaction = 0.25 * g * mass

    # Расстояния от центра масс до осей
    arm_forward = 0.5  # до оси передних колес
    arm_rear = 0.5  # до оси задних колес

    # Геометрия кузова
    assert arm_forward + arm_rear == BaseCar.width, "Geometry mismatch"

    # Момент инерции машинки относительно оси, проходящей через центр масс
    J = 82.0
    inv_J = 1.0 / J

    # Суммарные моменты инерции колес относительно осей вращения
    J_F = 0.5  # передних колес
    J_R = J_F  # задних колес
    inv_J_F = 1.0 / J_F
    inv_J_R = 1.0 / J_R

    # Радиусы колес
    wheel_forward_radius = 0.3
    wheel_rear_radius = 0.3

    # Параметры электродвигателя
    h = 22.5
    T = 0.015  # постоянная времени
    inv_T = 1.0 / T
    M_max = 150.0

    # Коэффициенты сопротивления
    C1 = 5600.0  # крипу
    C2 = 5600.0  # уводу

    def __init__(self, identifier, *initial_state, **kwargs):
        BaseCar.__init__(self)
        BaseObject.__init__(self, identifier, *initial_state)

        self.integrator.set_initial_value(self.state, kwargs.get("time", 0))
        self.friction = kwargs.get("friction", None)

        # assert self.friction is not None, "Friction not defined"
        assert isinstance(self.friction, TrackFrictionApproximation), "Friction should be a callable object"

        # self.sliding_data = np.zeros((9,), dtype=np.float)

    def _get_reactions(self, x, y, v1, omega_radius, vs1, vs2):
        """
        :param v1: продольная скорость колеса
        :param omega_radius: скорость вращения * радиус
        :param vs1: продольная скорость скольжения
        :param vs2: поперечная скосроть скольжения
        """

        abs_len_speed = abs(v1)
        abs_omega_radius = abs(omega_radius)
        abs_sliding_speed = np.hypot(vs1, vs2)

        reactions = np.array([[0, np.inf], [0, np.inf]], dtype=np.float, copy=False)

        # Силы реакций при полном проскальзывании
        if abs_sliding_speed > 0.0:
            # noinspection PyCallingNonCallable
            len_friction_factor, cross_friction_factor = self.friction(
                x, y, abs_sliding_speed, abs_len_speed, abs_omega_radius
            )
            var = - self.normal_reaction / abs_sliding_speed
            reactions[0, 0] = var * vs1 * len_friction_factor
            reactions[1, 0] = var * vs2 * cross_friction_factor

        # Силы реакций при деформируемой переферии
        if abs_len_speed > 0.0:
            if abs_omega_radius > abs_len_speed:
                reactions[0, 1] = - self.C1 * vs1 / abs_omega_radius
            else:
                reactions[0, 1] = - self.C1 * vs1 / abs_len_speed
            reactions[1, 1] = - self.C2 * vs2 / abs_len_speed
        elif abs_omega_radius > 0.0:
            reactions[0, 1] = - self.C1 * vs1 / abs_omega_radius

        # Выбираем наименьшие по модулю
        abs_reactions = np.abs(reactions)
        return reactions[0, abs_reactions[0, :].argmin()], reactions[1, abs_reactions[1, :].argmin()]

    def _process(self, t, state, controls):
        moment, wheel_turn = controls  # управления

        # Направление поворота ведущих колес
        wtx, wty = np.cos(state[9]), np.sin(state[9])

        alf = state[4] + state[5] * self.arm_forward

        # Скорости колес
        v1_f = state[3] * wtx + alf * wty  # продольная скорость передних колес
        vs2_f = alf * wtx - state[3] * wty  # поперечная скорость передних колес
        v1_r = state[3]  # продольная скорость задних колес
        vs2_r = state[4] - state[5] * self.arm_rear  # поперечная скорость задних колес

        # Радиус * скорость вращения
        omega_radius_f = state[7] * self.wheel_forward_radius  # передних
        omega_radius_r = state[6] * self.wheel_rear_radius  # задних

        # Продольные скорости скольжения колес
        vs1_f = v1_f - omega_radius_f  # передних
        vs1_r = v1_r - omega_radius_r  # задних

        # Силы реакции
        r1_f, r2_f = self._get_reactions(state[0], state[1], v1_f, omega_radius_f, vs1_f, vs2_f)  # передних колес
        r1_r, r2_r = self._get_reactions(state[0], state[1], v1_r, omega_radius_r, vs1_r, vs2_r)  # задних колес

        # Текущее направление робота
        ax, ay = np.cos(state[2]), np.sin(state[2])

        # Сохранение характеристик скольжения (замедляет... примерно в 8 раз)
        # self.sliding_data = np.vstack((self.sliding_data, [t, vs1_f, vs2_f, vs1_r, vs2_r, r1_f, r2_f, r1_r, r2_r]))

        # Изменение состояния робота
        d_dt = np.zeros_like(state)
        d_dt[0] = state[3] * ax - state[4] * ay
        d_dt[1] = state[3] * ay + state[4] * ax
        d_dt[2] = state[5]
        d_dt[3] = state[5]*state[4] + self.inverted_mass*(r1_r + r1_f*wtx - r2_f*wty)
        d_dt[4] = -state[5]*state[3] + self.inverted_mass*(r2_r + r1_f*wty + r2_f*wtx)
        d_dt[5] = self.inv_J*((r1_f*wty + r2_f*wtx)*self.arm_forward - r2_r*self.arm_rear)
        d_dt[6] = self.inv_J_R*(state[8] - r1_r*self.wheel_rear_radius)
        d_dt[7] = -self.inv_J_F*r1_f*self.wheel_forward_radius
        d_dt[8] = self.inv_T*(moment - self.h*state[6]-state[8])

        # Расчет изменения поворота колес
        dwt = wheel_turn - state[9]  # Нужный угол поворота колес
        d_dt[9] = 0.0 if abs(dwt) < TOLERANCE else self.max_wheel_turn_change * np.sign(dwt)

        return d_dt

    def move(self, t_max, *args, **kwargs):

        assert -BaseCar.max_wheel_turn <= args[1] <= BaseCar.max_wheel_turn, \
            "Bad wheel_turn %s (max:%s)" % (args[1], self.max_wheel_turn)
        assert -self.M_max <= args[0] <= self.M_max, \
            "Bad moment %s (M_max:%s)" % (args[0], self.M_max)
        assert self.integrator.t < t_max, \
            "Integrator time mismatch: %s >= %s" % (self.integrator.t, t_max)

        self.integrator.set_f_params(args)

        self.state = self.integrator.integrate(t_max)

        return self.integrator.successful()


class MaterialPoint(RectangularObject, BaseCar):

    _state_props = (
        "x",
        "y",
        "angle"
    )

    # noinspection PyCallByClass
    def __init__(self, identifier, *initial_state, **kwargs):
        BaseCar.__init__(self)
        BaseObject.__init__(self, identifier, *initial_state)

        self.integrator.set_initial_value(self.state, kwargs.get("time", 0))

    def _process(self, t, state, controls):

        d_dt = np.zeros_like(state)

        d_dt[0] = controls[0] * np.cos(state[2])
        d_dt[1] = controls[0] * np.sin(state[2])
        d_dt[2] = controls[1]

        return d_dt

    def move(self, t_max, *args, **kwargs):
        assert len(args) == 2
        assert abs(args[0]) <= self.max_speed

        self.integrator.set_f_params(args)

        self.state = self.integrator.integrate(t_max)

        return self.integrator.successful()