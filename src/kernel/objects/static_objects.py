# coding=utf-8
"""
Данный модуль содержит базовые классы для определения объектов и коллизий между ними
"""
import numpy as np

# Local imports
from src.kernel.objects.bases import BaseObject, ObjectMeta
from src.kernel.plot import LINE_WIDTH, DEFAULT_LINE


# noinspection PyUnresolvedReferences,PyAbstractClass
class BaseStaticObject(BaseObject):
    """
    Базовый класс для объектов
    """

    _state_props = ("x", "y")
    __metaclass__ = ObjectMeta

    def __init__(self, identifier, *args):
        """
        :param identifier: идентификатор объекта
        :type identifier: str

        :param x: абсцисса центра объекта
        :type x: float

        :param y: ордината центра объекта
        :type y: float
        """

        super(BaseStaticObject, self).__init__(identifier, *args)

    def get_distance_to(self, x, y):
        """
        Возвращает расстояние до полученных координат
        """
        return np.hypot(x - self.x, y - self.y)

    def get_distance_to_object(self, obj):
        """
        Возвращает расстояние до центра другого объекта

        :type obj: BaseStaticObject
        :rtype: np.float64
        """
        return self.get_distance_to(obj.x, obj.y)

    def get_displacement_to(self, x, y):
        """
        Возвращает смещение относительно полученных координат
        """
        return x - self.x, y - self.y

    def get_displacement_to_object(self, obj):
        """
        Возвращает смещение относительно центра другого объекта

        :type obj: BaseStaticObject
        :rtype: np.float64
        """
        return self.get_displacement_to(obj.x, obj.y)

    def plot_mpl(self, axis, **kwargs):
        axis.plot([self.x, self.y])


# noinspection PyUnresolvedReferences,PyAbstractClass
class OrientedObject(BaseStaticObject):
    """
    Базовое представление ориентированных объектов
    """

    _state_props = ("x", "y", "angle")

    __metaclass__ = ObjectMeta

    def __init__(self, identifier, *args):
        """
        :param identifier: идентификатор объекта
        :type identifier: str

        :param x: абсцисса центра объекта
        :type x: float

        :param y: ордината центра объекта
        :type y: float

        :param angle: угол поворота объекта
        :type angle: float
        """

        super(OrientedObject, self).__init__(identifier, *args)

    @property
    def direction(self):
        return np.cos(self.angle), np.sin(self.angle)

    def get_angle_to(self, x, y):

        absolute_angle_to = np.arctan2(y - self.y, x - self.x)

        disorientation = np.array(self.angle - absolute_angle_to, dtype=np.float)

        while disorientation > np.pi:
            disorientation -= 2.0 * np.pi

        while disorientation < -np.pi:
            disorientation += 2.0 * np.pi

        return disorientation


# noinspection PyUnresolvedReferences
class RectangularObject(OrientedObject):
    """
    Базовое представление прямоугольных объектов
    """

    _state_props = ("x", "y", "angle")

    __metaclass__ = ObjectMeta

    def __init__(self, identifier, x, y, angle, width, height):
        """
        :param identifier: идентификатор объекта
        :type identifier: str

        :param x: абсцисса центра объекта
        :type x: float

        :param y: ордината центра объекта
        :type y: float

        :param angle: угол поворота объекта
        :type angle: float

        :param width: ширина объекта
        :type width: float

        :param height: высота объекта
        :type height: float
        """

        super(RectangularObject, self).__init__(identifier, x, y, angle)

        self.width = width
        self.height = height

        half_width, half_height = 0.5 * width, 0.5 * height

        self.borders = np.array([
            [-half_width, half_height],
            [half_width, half_height],
            [-half_width, -half_height],
            [half_width, - half_height]
        ], dtype=np.float)

    def get_borders(self):
        ax, ay = self.direction
        half_width, half_height = 0.5 * self.width, 0.5 * self.height

        static_borders_x = np.array([-half_width, half_width, half_width, -half_width])
        static_borders_y = np.array([half_height, half_height, -half_height, -half_height])

        borders_x = self.x + static_borders_x * ax + static_borders_y * ay
        borders_y = self.y + static_borders_x * ay - static_borders_y * ax

        # borders_x = self.x + static_borders_x * ax - static_borders_y * ay
        # borders_y = self.y + static_borders_x * ay + static_borders_y * ax

        return np.hstack((borders_x.reshape(borders_x.size, 1), borders_y.reshape(borders_y.size, 1)))

    def plot_mpl(self, axis, **kwargs):
        borders = self.get_borders()

        line = kwargs.get("linestyle", DEFAULT_LINE) + kwargs.get("marker", "")
        linewidth = kwargs.get("linewidth", LINE_WIDTH)

        axis.plot(borders[0:2, 0], borders[0:2, 1], line, linewidth=linewidth)
        axis.plot(borders[1:3, 0], borders[1:3, 1], line, linewidth=linewidth)
        axis.plot(borders[2:4, 0], borders[2:4, 1], line, linewidth=linewidth)
        axis.plot([borders[3, 0], borders[0, 0]], [borders[3, 1], borders[0, 1]], line, linewidth=linewidth)

        # for point in borders:
        #     axis.plot(point[0], point[1], "ko")


if __name__ == '__main__':
    o = RectangularObject("object", 5.0, 5.0, 0.5*np.pi, 1.0, 0.25)

    d = o.get_distance_to(1.0, 1.0)
    print d, type(d)

    x_array = np.array([0.0, 1.0, 2.0])
    y_array = np.array([0.0, 1.0, 2.0])

    d_array = o.get_distance_to(x_array, y_array)
    print d_array, type(d_array)

    displacement = o.get_displacement_to(1.0, 2.0)
    print displacement, type(displacement)

    dx, dy = o.get_displacement_to(x_array, y_array)
    print dx, dy, type(dx), type(dy)

    d_angle = o.get_angle_to(1.0, 1.0)
    print d_angle, type(d_angle)

    # d_angle_array = o.get_angle_to(x_array, y_array)
    # print d_angle_array, type(d_angle_array)

    import matplotlib.pyplot as plt

    o.plot_mpl(plt, linewidth="2", marker="o")
    plt.xlim([0, 10])
    plt.ylim([0, 10])
    plt.show()
