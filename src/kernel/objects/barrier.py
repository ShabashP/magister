# coding=utf-8
import numpy as np
from scipy.spatial.distance import cdist

__all__ = (
    "Barrier",
    "CenteredBarrier",
    "CenteredRotatedBarrier",
    "CenteredMutableBarrier",
    "create_barrier",
    "BARRIER_TYPE_01_POINTS",
    "BARRIER_TYPE_02_POINTS",
    "BARRIER_TYPE_03_POINTS",
    "BARRIER_TYPE_04_POINTS",
    "BARRIER_TYPE_05_POINTS",
    "BARRIER_TYPE_06_POINTS",
    "BARRIER_TYPE_08_POINTS",
)


FACE_COLOR = "green"


class Barrier(object):

    def collide(self, x, y):
        raise NotImplementedError

    def plot_mpl(self, axis):
        raise NotImplementedError


class CenteredBarrier(Barrier):

    n_points = 100

    def __init__(self, x0, y0, points):

        assert points.shape[1] == 2, "Not points"
        assert points[0, 0] == points[-1, 0] and points[0, 1] == points[-1, 1], "Not area"

        self.x0 = x0
        self.y0 = y0

        n = int(self.n_points/points.shape[0])
        x, y = [], []
        for i in xrange(1, points.shape[0]):
            # if points[i-1][0] == points[i][0]:
            #     xi = points[i][0] * np.ones((n, ))
            #     y.extend(np.linspace(min(points[i-1:i+1, 0]), max(points[i-1:i+1, 0]), num=n, endpoint=True))
            # else:
            p = np.polyfit(x=points[i-1:i+1, 0], y=points[i-1:i+1, 1], deg=1)
            xi = np.linspace(points[i-1, 0], points[i, 0], num=n, endpoint=True)
            y.extend(np.polyval(p, xi))
            x.extend(xi)

        p = np.column_stack((x, y))
        dy, dx = p[:, 1]-self.y0, p[:, 0]-self.x0
        angle = np.arctan2(dy, dx)

        # noinspection PyTypeChecker
        indices = np.argwhere(angle < 0)
        if indices.size > 0:
            angle[indices] += 2.0*np.pi

        d = np.hypot(dx, dy)
        self.data = np.column_stack((p, angle, d))

    def plot_mpl(self, axis, face_color=FACE_COLOR):
        # axis.plot(self.data[:, 0], self.data[:, 1], "k-")
        axis.fill_between(self.data[:, 0], 0.0, self.data[:, 1], facecolor=face_color, linewidth=0.0)

    def collide(self, x, y):

        dy, dx = y - self.y0, x - self.x0
        angle = np.arctan2(dy, dx)

        if angle < 0.0:
            angle += 2.0*np.pi

        i = np.abs(self.data[:, 2] - angle).argmin()

        return np.hypot(dx, dy) <= self.data[i, 3]

    def distance(self, x, y):
        return cdist(self.data[:, 0:2], np.array([[x, y], ])).min()


class CenteredRotatedBarrier(CenteredBarrier):

    def __init__(self, x0, y0, angle, points):
        ax, ay = np.cos(angle), np.sin(angle)
        new_points = np.zeros_like(points)
        dx, dy = points[:, 0] - x0, points[:, 1] - y0
        new_points[:, 0] = x0 + dx*ax - dy*ay
        new_points[:, 1] = y0 + dx*ay + dy*ax
        super(CenteredRotatedBarrier, self).__init__(x0, y0, new_points)

    def collide(self, x, y):
        return super(CenteredRotatedBarrier, self).collide(x, y)

    def plot_mpl(self, axis, face_color=FACE_COLOR):
        super(CenteredRotatedBarrier, self).plot_mpl(axis, face_color)


class CenteredMutableBarrier(CenteredRotatedBarrier):

    def __init__(self, x0, y0, points, scale, angle):
        initial_position = np.array([[x0, y0], ])
        points -= initial_position
        points *= scale
        points += initial_position

        super(CenteredMutableBarrier, self).__init__(x0, y0, angle, points)

    def collide(self, x, y):
        return super(CenteredMutableBarrier, self).collide(x, y)

    def plot_mpl(self, axis, face_color=FACE_COLOR):
        super(CenteredMutableBarrier, self).plot_mpl(axis, face_color)


BARRIER_TYPE_01_POINTS = np.array([
    (3.0, 0.0),
    (1.0, 1.0),
    (0.0, 2.0),
    (-1.25, 1.0),
    (-2.0, 0.0),
    (-1.0, -1.0),
    (0.0, -1.0),
    (1.5, -1.25),
    (2.5, -0.75),
    (3.0, 0.0),
])


BARRIER_TYPE_02_POINTS = np.array([
    (0.5, 0.0),
    (1.0, 0.25),
    (0.75, 0.5),
    (0.0, 0.35),
    (-0.5, -0.25),
    (-0.30, -0.45),
    (0.3, -0.25),
    (0.5, 0.0),
])


BARRIER_TYPE_03_POINTS = np.array([
    (2.75, 0.0),
    (1.75, 0.85),
    (1.25, 1.25),
    (0.5, 0.75),
    (0.0, 0.65),
    (-0.5, 0.65),
    (-0.75, 0.45),
    (-1.0, 0.0),
    (-1.5, -0.75),
    (-1.45, -1.05),
    (-1.35, -1.25),
    (-0.85, -1.65),
    (-0.10, -1.75),
    (0.65, -1.35),
    (1.5, -0.25),
    (2.75, 0.0)
])


BARRIER_TYPE_04_POINTS = np.array([
    (0.8, 0.0),
    (0.75, 1.0),
    (0.25, 1.5),
    (-0.3, 0.5),
    (-0.45, 0.0),
    (-0.3, -0.5),
    (0.25, -1.5),
    (0.75, -1.0),
    (0.8, 0.0),
])


BARRIER_TYPE_05_POINTS = np.array([
    (0.3, 0.0),
    (0.2, 0.55),
    (0.15, 0.75),
    (0.05, 0.8),
    (-0.1, 0.85),
    (-0.2, 0.7),
    (-0.4, 0.0),
    (-0.2, -0.7),
    (-0.1, -0.85),
    (0.05, -0.8),
    (0.15, -0.75),
    (0.2, -0.55),
    (0.3, 0.0),
])


BARRIER_TYPE_06_POINTS = np.array([
    (3.25, 0.0),
    (3.0, 0.5),
    (2.0, 0.75),
    (1.5, 0.80),
    (1.0, 1.0),
    (0.0, 2.0),
    (-1.25, 1.0),
    (-2.0, 0.0),
    (-1.0, -1.0),
    (0.0, -1.50),
    (1.5, -1.25),
    (2.5, -0.75),
    (3.25, 0.0),
])


BARRIER_TYPE_08_POINTS = np.array([
    (3.25, 0.0),
    (3.0, 0.5),
    (2.0, 0.75),
    (1.5, 0.80),
    (1.0, 1.0),
    (-0.5, 3.5),
    (-1.5, 3.75),
    (-2.5, 3.20),
    (-2.0, 0.0),
    (-1.0, -1.0),
    (0.0, -1.50),
    (1.5, -1.25),
    (2.5, -0.75),
    (3.25, 0.0),
])


def create_barrier(x, y, points, scale=1.0, angle=0.0):
    return CenteredMutableBarrier(x, y, points + np.array([[x, y], ]), scale, angle)


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    S = 1.05
    X0, Y0 = 0.0, 0.0
    POINTS = BARRIER_TYPE_05_POINTS + np.array([[X0, Y0], ])

    EXT_POINTS = np.array([
        (0.0, 0.0),
        (0.0, S),
        (0.0, -S),
        (S, S),
        (S, -S),
        (S, 0),
        (-S, 0),
        (-S, -S),
        (-S, S),
    ]) + np.array([[X0, Y0], ])

    barrier = CenteredMutableBarrier(X0, Y0, POINTS, scale=1.0, angle=0.0*np.pi)
    barrier.plot_mpl(plt)

    for ext_point in EXT_POINTS:
        if barrier.collide(*ext_point):
            plt.plot(ext_point[0], ext_point[1], "bx", markersize=8.0)
        else:
            plt.plot(ext_point[0], ext_point[1], "bo", markersize=8.0)
            print "%s -- distance=%s" % (ext_point, barrier.distance(*ext_point))
    plt.show()