# coding=utf-8
import numpy as np


class BaseObject(object):
    """
    Базовый класс для объектов
    """

    # Кортеж, содержащий имена динамически создаваемых
    # атрибутов объекта (создаются при инициализации класса),
    # ассоциированных с параметрами состояния объекта (см. класс ObjectMeta)
    _state_props = ()

    def __init__(self, identifier=None, *args):
        """
        :param identifier: идентификатор объекта
        :param args: начальное значение вектора состояния объекта
        """
        self.id = identifier
        self.state = np.array(args, dtype=np.float)

    def plot_mpl(self, axis, **kwargs):
        raise NotImplementedError

    def get_borders(self):
        raise NotImplementedError


class StateDescriptor(object):

    def __init__(self, index):
        self.index = index

    def __get__(self, instance, owner):
        assert issubclass(owner, BaseObject)  # fixme debug
        return instance.state[self.index]

    def __set__(self, instance, value):
        instance.state[self.index] = value

    def __delete__(self, instance):
        raise AttributeError("Permission denied")


class ObjectMeta(type):

    def __new__(mcs, name, bases, dct):
        """
        :type name: str
        :type bases: tuple
        :type dct: dict
        """
        state_properties = dct.pop("_state_props")

        for i in xrange(0, len(state_properties)):
            dct[state_properties[i]] = StateDescriptor(i)
        return super(ObjectMeta, mcs).__new__(mcs, name, bases, dct)