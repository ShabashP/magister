# coding=utf-8

# Plotting configuration
from matplotlib import pyplot as plt

FONT_SIZE = 16.0
LEGEND_SIZE = 10.0
LINE_WIDTH = 1.75
ONE_LINE = 1.0
GRID_STYLE = "--"
GRID_WIDTH = 1.0
DEFAULT_LINE = "k-"
EXT_LINE = "b-"
THIRD_LINE = "g-"
DEFAULT_MARKER = "ko"
MARKER_SIZE = 5.0


def new_figure():
    f = plt.figure(figsize=(8.0, 5.0))
    p = f.add_subplot(1, 1, 1)
    return f, p


def save_figure(directory, fig, title, close_fig=True):
    fig.savefig("%s%s" % (directory, title))
    if close_fig:
        plt.close(fig)


def grid_for_axis(axis, grid=True):
    for ax in axis:
        ax.grid(grid)


