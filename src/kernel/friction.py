# coding=utf-8
from src.utils import linear_interpolation
# from src.kernel.track.track import get_tile_index

__all__ = (
    "FrictionFactorApproximation",
    "very_good_traction",
    "very_bad_traction",
    "good_traction",
    "bad_traction",
    "normal_traction",
    "TrackFrictionApproximation"
)


class FrictionFactorApproximation(object):

    min_meaningful_sliding_speed = 0.005

    def __init__(self, static_friction_factor, sliding_friction_factor, sliding_factor):
        self.sliding_factor = sliding_factor
        self.static_friction = static_friction_factor
        self.sliding_friction = sliding_friction_factor

    def __calc(self, abs_sliding_speed, v):
        # assert v > self.min_meaningful_sliding_speed, "I don\'t know.. %s < %s" % (v, self.min_meaningful_sliding_speed)

        if abs_sliding_speed <= self.min_meaningful_sliding_speed:
            return linear_interpolation(
                x=abs_sliding_speed,
                x1=0.0, y1=0.0, x2=self.min_meaningful_sliding_speed, y2=self.static_friction
            )
        elif self.min_meaningful_sliding_speed < abs_sliding_speed < v:
            return linear_interpolation(
                x=abs_sliding_speed,
                x1=self.min_meaningful_sliding_speed, y1=self.static_friction, x2=v, y2=self.sliding_friction
            )
        return self.sliding_friction

    def __call__(self, abs_sliding_speed, abs_len_speed, abs_omega_radius):

        return self.__calc(abs_sliding_speed, self.sliding_factor * max([abs_len_speed, abs_omega_radius])), \
               self.__calc(abs_sliding_speed, self.sliding_factor * abs_len_speed)

    def __eq__(self, other):
        return self.sliding_friction == other.sliding_friction and \
               self.static_friction == other.static_friction and \
               self.sliding_factor == other.sliding_factor


very_good_traction = FrictionFactorApproximation(0.6, 0.3, 0.1)
good_traction = FrictionFactorApproximation(0.4, 0.2, 0.1)
normal_traction = FrictionFactorApproximation(0.3, 0.15, 0.1)
bad_traction = FrictionFactorApproximation(0.15, 0.075, 0.1)
very_bad_traction = FrictionFactorApproximation(0.1, 0.055, 0.1)


class TrackFrictionApproximation(object):

    def __init__(self, default_friction):
        """
        :type default_friction: FrictionFactorApproximation
        """
        self.default_friction = default_friction
        self.extra_friction = {}

    def add_extra(self, i, j, friction):
        """
        :type i: int
        :type j: int
        :type friction: FrictionFactorApproximation
        """
        self.extra_friction[(i, j)] = friction

    def __call__(self, x, y, abs_sliding_speed, abs_len_speed, abs_omega_radius):
        # FIXME как???
        # i, j = get_tile_index(x, y)
        #
        # if (i, j) in self.extra_friction:
        #     return self.extra_friction[(i, j)](abs_sliding_speed, abs_len_speed, abs_omega_radius)
        return self.default_friction(abs_sliding_speed, abs_len_speed, abs_omega_radius)

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import numpy as np

    f = very_bad_traction

    vs_range = np.linspace(0.0, 3.0, 100)

    u = np.array([f(vs, 1.0, 3.5) for vs in vs_range])

    plt.plot(vs_range, u[:, 0])

    plt.figure()
    plt.plot(vs_range, u[:, 1])

    plt.show()