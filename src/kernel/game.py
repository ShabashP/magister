# coding=utf-8
import numpy as np
from src.kernel.track.maps import *

__all__ = (
    "Game01",
    "Game02"
)


class Game(object):

    track_map_cls = None
    waypoints = None
    orientation = 0.0
    K_v = 0.0

    def __init__(self, min_pass_distance):

        assert issubclass(self.track_map_cls, Map)

        self.track_map = self.track_map_cls(self.waypoints, min_pass_distance)

        self.initial_state = [
            self.track_map.waypoints[0][0],
            self.track_map.waypoints[0][1],
            self.orientation
        ]

    def get_args(self):
        return self.track_map, self.initial_state


class Game01(Game):
    track_map_cls = Map01
    waypoints = WAYPOINTS_MAP01_01
    orientation = 0.5*np.pi


class Game02(Game01):
    track_map_cls = Map02
    waypoints = WAYPOINTS_MAP02_01
