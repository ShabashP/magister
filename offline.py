# coding=utf-8
"""
Только синтез траекторий
"""
import os
import shutil
from src.calculator import base_plot, plot_trajectory
from src.synthesizer import Synthesizer
from src.kernel.game import *

BASE_DIR = "img/"
TILE_SIZE = 5.0

MIN_PASS_DISTANCE = 0.5*TILE_SIZE

game = Game02(MIN_PASS_DISTANCE)

if __name__ == '__main__':

    track_map, initial_state = game.get_args()
    out_dir = BASE_DIR + "offline_%s/" % track_map.__class__.__name__

    if os.path.isdir(out_dir):
        shutil.rmtree(out_dir)

    synthesizer = Synthesizer(track_map, TILE_SIZE, MIN_PASS_DISTANCE, *initial_state)

    print "Created %s routes" % len(synthesizer.tile_routes)

    trajectories = synthesizer.create_trajectories()

    os.makedirs(out_dir)

    base_plot((track_map, synthesizer, trajectories, out_dir), )
    for trajectory in trajectories:
        d = out_dir + trajectory.identificator + "/"
        os.makedirs(d)
        plot_trajectory((track_map, synthesizer, trajectory, d))
