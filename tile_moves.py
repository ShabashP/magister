# coding=utf-8
from src.synthesizer import *


TYPES = range(0, 14)

if __name__ == '__main__':
    for maneuver_type, maneuver in MANEUVERS.iteritems():
        print "Maneuver type: %s" % maneuver_type
        t = list(TYPES)
        t.remove(TileType.EMPTY)
        for mnt in maneuver.type_from:
            t.remove(mnt)
        print "from: %s" % t
        t = list(TYPES)
        t.remove(TileType.EMPTY)
        for mnt in maneuver.type_to:
            t.remove(mnt)
        print "to: %s" % t
