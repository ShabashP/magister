# coding=utf-8
import os
import time
import multiprocessing as mp
import numpy as np

from collections import OrderedDict
from src.calculator import calculate, base_plot, plot_trajectory
from src.synthesizer import Synthesizer
from src.post import PostProcessor
from src.kernel.friction import *
from src.kernel.game import *

BASE_DIR = "img/"
TILE_SIZE = 5.0

MIN_PASS_DISTANCE = 0.5*TILE_SIZE

games = (
    Game02(MIN_PASS_DISTANCE),
)

FRICTION = OrderedDict()
# FRICTION["01_very_good_traction"] = TrackFrictionApproximation(very_good_traction)
FRICTION["02_normal_traction"] = TrackFrictionApproximation(normal_traction)
FRICTION["03_bad_traction"] = TrackFrictionApproximation(bad_traction)
FRICTION["04_very_bad_traction"] = TrackFrictionApproximation(very_bad_traction)

TRAJECTORIES = (
    "BaseSplineTrajectory",
    "BaseLinearTrajectory",
    "BaseKinematicTrajectory",
    "KinematicTrajectory"
)


def main(game):
    t0 = time.time()
    track_map, initial_state = game.get_args()
    out_dir = BASE_DIR + time.strftime("%Y_%m_%d_%H_%M_%S", time.gmtime()) + "_%s/" % game.__class__.__name__
    os.makedirs(out_dir)

    synthesizer = Synthesizer(track_map, TILE_SIZE, MIN_PASS_DISTANCE, *initial_state)
    trajectories = synthesizer.create_trajectories()

    pool = mp.Pool(mp.cpu_count()-1)
    sim_args, t_args = [], []
    pool.map(base_plot, [(track_map, synthesizer, trajectories, out_dir), ])

    for trajectory in trajectories:
        name = out_dir + trajectory.identificator
        t = name + "/"
        os.makedirs(t)
        t_args.append((track_map, synthesizer, trajectory, t))
        for friction_name, friction in FRICTION.iteritems():
            d = name + "_" + friction_name + "/"
            os.makedirs(d)
            sim_args.append(
                (track_map, trajectory, initial_state, friction, MIN_PASS_DISTANCE, d, friction_name, game.K_v)
            )

    pool.map(plot_trajectory, t_args)
    sim_data = pool.map(calculate, sim_args)
    pool.close()
    pool.join()
    calculation_time = time.time() - t0

    # ----------------------------------------------------------------------------------------------------------------
    # Write data to file
    # ----------------------------------------------------------------------------------------------------------------
    best_results = []
    with open(out_dir + "data.txt", "w+") as f:
        f.write("Calculation time: %s \n" % calculation_time)
        f.write("Created %s routes. Weight: %s \n\n" % (len(synthesizer.routes), min(synthesizer.weights)))

        for trajectory in trajectories:
            f.write(
                "%s \t time: %s \t max_angular_speed: %s \n" % (
                    trajectory.identificator, trajectory.duration, np.abs(trajectory.angular_speed).max()
                )
            )
        f.write("\n")
        for result in sim_data:
            identificator, calculation_time, p = result
            assert isinstance(p, PostProcessor)
            f.write("%s data: \n" % identificator)
            f.write(
                "Program time: %s \t Real time: %s \t Overtime: %s percents\n" % (
                    p.program_time, p.simulation_time, p.time_over
                )
            )
            f.write(
                "Controls: \t K_v=%s \t K_angle=%s \t K_coord=%s \t K_p=%s \t K_i=%s \n" % (
                    p.K_v, p.K_angle, p.K_coord, p.K_p, p.K_i
                )
            )
            f.write("Maximum errors: \t e_angle=%s \t e_coord=%s \n" % (p.max_e_angle, p.max_e_coord))
            f.write("Averaged errors: \t e_angle=%s \t e_coord=%s \n" % (p.av_e_angle, p.av_e_coord))
            f.write("Max sliding lengthwise speed: \t v_f=%s \t v_r=%s \n" % (p.max_vs1_f, p.max_vs1_r))
            f.write("Max sliding crosswise speed: \t v_f=%s \t v_r=%s \n" % (p.max_vs2_f, p.max_vs2_r))
            f.write("Average sliding lengthwise speed: \t v_f=%s \t v_r=%s \n" % (p.av_vs1_f, p.av_vs1_r))
            f.write("Average sliding crosswise speed: \t v_f=%s \t v_r=%s \n" % (p.av_vs2_f, p.av_vs2_r))
            best_results.append(
                (
                    p.time_over, p.max_e_angle, p.max_e_coord, p.av_e_angle, p.av_e_coord,
                    p.max_vs1_f, p.max_vs1_r, p.max_vs2_f, p.max_vs2_r, p.av_vs1_f, p.av_vs1_r, p.av_vs2_f, p.av_vs2_r
                )
            )
            f.write("\n\n")

        f.write("Comparison: \n")
        f.write("\t".join((
            "Overtime", "Max e_angle", "Max e_coord", "Av e_angle", "Av e_coord",
            "Max vs1_f", "Max vs1_r", "Max vs2_f", "Max vs2_r", "Average vs1_f", "Average vs1_r",
            "Average vs2_f", "Average vs2_r"
        )))
        f.write("\n")
        for row in best_results:
            s = []
            for element in row:
                s.append("%3.4f" % element)
            f.write(" \t ".join(s) + "\n")
        r = np.array(best_results)
        f.write("\nBest results: \n")
        f.write("Overtime: %s \n" % sim_data[r[:, 0].argmin()][0])
        f.write("Max angle error: %s \n" % sim_data[r[:, 1].argmin()][0])
        f.write("Max coordinate error: %s \n" % sim_data[r[:, 2].argmin()][0])
        f.write("Average angle error: %s \n" % sim_data[r[:, 3].argmin()][0])
        f.write("Average coordinate error: %s \n" % sim_data[r[:, 4].argmin()][0])
        f.write("Maximum forward wheels lengthwise sliding speed: %s \n" % sim_data[r[:, 5].argmin()][0])
        f.write("Maximum rear wheels lengthwise sliding speed: %s \n" % sim_data[r[:, 6].argmin()][0])
        f.write("Maximum forward wheels crosswise sliding speed: %s \n" % sim_data[r[:, 7].argmin()][0])
        f.write("Maximum rear wheels crosswise sliding speed: %s \n" % sim_data[r[:, 8].argmin()][0])

    data = {t: {} for t in TRAJECTORIES}
    for result in sim_data:
        identificator, calculation_time, p = result
        assert isinstance(identificator, str)
        t, f = identificator.split("___")
        data[t][f] = p

    with open(out_dir + "tables.tex", "w+") as f:

        line_headers = [
            "$t$ ",  # 0
            "$d_{min}$",
            "$ \\langle V_{S_1} \\rangle $ ",  # 2
            "$ \\langle V_{S_2} \\rangle $ ",  # 3
            "$ V_{S_1}^{max} $ ",  # 4
            "$ V_{S_2}^{max} $ ",  # 5
            "$ t / t^r $ ",  # 6
            "$ \\langle e_{y} \\rangle $ ",  # 7
            "$ \\langle e_{\\varphi} \\rangle $ ",  # 8
            "$ e_{y}^{max} $ ",  # 9
            "$ e_{\\varphi}^{max} $ "  # 10
        ]
        for fr in FRICTION.iterkeys():
            f.write(fr + "\n")
            lines = list(line_headers)
            for t in TRAJECTORIES:
                p = data[t][fr]
                assert isinstance(p, PostProcessor)
                lines[0] += "\t & \t %3.2f" % p.simulation_time
                lines[1] += "\t & \t %3.4f" % p.min_d
                lines[2] += "\t & \t $V_{S_1}^F = %3.4f$ $V_{S_1}^R = %3.4f$" % (p.av_vs1_f, p.av_vs1_r)
                lines[3] += "\t & \t $V_{S_2}^F = %3.4f$ $V_{S_2}^R = %3.4f$" % (p.av_vs2_f, p.av_vs2_r)
                lines[4] += "\t & \t $V_{S_1}^F = %3.4f$ $V_{S_1}^R = %3.4f$" % (p.max_vs1_f, p.max_vs1_r)
                lines[5] += "\t & \t $V_{S_2}^F = %3.4f$ $V_{S_2}^R = %3.4f$" % (p.max_vs2_f, p.max_vs2_r)
                lines[6] += "\t & \t %3.4f" % p.time_over
                lines[7] += "\t & \t %3.4f" % p.av_e_coord
                lines[8] += "\t & \t %3.4f" % p.av_e_angle
                lines[9] += "\t & \t %3.4f" % p.max_e_coord
                lines[10] += "\t & \t %3.4f" % p.max_e_angle
            for s in lines:
                s += "\t \\\\ \\hline \n"
                f.write(s)
            f.write("\n\n\n\n\n")


if __name__ == '__main__':

    for g in games:
        main(g)
